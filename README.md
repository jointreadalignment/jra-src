Welcome to the Joint Read Aligner (JRA) !

Please visit our wiki for installation and usage information:
[https://bitbucket.org/jointreadalignment/jra-src/wiki/Home](https://bitbucket.org/jointreadalignment/jra-src/wiki/Home)
