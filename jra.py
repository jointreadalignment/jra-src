#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""wrapper for running jra from source tree"""

from jra.jra import main

if __name__ == '__main__':
    main()
