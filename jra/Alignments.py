#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
contains classes and methods for manipulating alignments/mappings
"""

from itertools import groupby,chain
import math
import pprint
from collections import namedtuple
from bitarray import bitarray

import logging


logger = logging.getLogger(__name__)

def dictFromStrings(strings):
    pairs = [i.split("=") for i in strings]
    return dict(pairs)

class Alignment:
  
    def __init__(self, **kwargs):   #this is bad.
       
        if "maf" in kwargs.keys(): #alignment info is being read in from a maf file. 
            
            lines = kwargs["maf"] # lines is a list of lines in a mafBlock. [[a score=...],[s ...],[s ...],[p ... ],,]
            lines = map(str.split, lines) 
            
            
            #getting "a" line info , ie. score and mismap
            aLines = [i[1:] for i in lines if i[0] == "a"] #extrac the line(s) starting with "a"
            namesAndValues = dictFromStrings(chain(*aLines)) # name=value (eg. score=500) will now be in a dictionary
            
            self.score=int(namesAndValues["score"])
            if 'mismap' in namesAndValues:
                self.LASTmismap = float(namesAndValues['mismap'])
            else:
                self.LASTmismap = None
                
            #getting "s" line info
            sLines = [i for i in lines if i[0] == "s"]
            if not sLines: raise Exception("empty alignment")
            if not len(sLines) == 2 : raise Exception("not pairwise alignment")
            
            refLine = sLines[0]
            queryLine = sLines[1]
            
            self.rID = queryLine[1]
            self.start = int(queryLine[2]) #read (query) start
            self.rAlnSize = int(queryLine[3])
            self.end = self.start + self.rAlnSize-1
            self.strand = queryLine[4] #assuming ref is always reported as +ve strand
            self.width = int(queryLine[5]) #ambiguous naming. legacy.
            self.querySeq = queryLine[6]
     
            
            self.gchr = refLine[1]
            self.gstart = int(refLine[2]) #reference start
            self.gend = self.gstart+int(refLine[3])-1
            self.gSeq = refLine[6]
            
            ##HUGUES MODIFICATION, we need that for easy MAF OUTPUT 
            self.gstrand = refLine[4]
            self.glen = int(refLine[5])

            
            self.overlappingIntervals = None #a list that will be filled later when doing interval query. 
            self.overlapIntv = None #one of the intervals from list above that this aln is supposed to correspond to. this could be modified during processing , so we will keep the flag below to tell us this aln was the first in that genomic location.
            #self.firstIntv = None #interval does not have any overlapping intervals. same info can be ontained from len of overlappingIntervals, but we modify the latter for easy processing at Split.updateCoordMaps. 
            self.coordMap = None # aln will contribute to a profile. coordMap provides mapping between coord of aln(ref + read) to coord of profile. once aln is processed, this is attribute is set to the dict that provided the mapping. useful when same alignment is processed for multiple splits.
            
        elif "colonSeparated" in kwargs.keys():
                    
            alignmentInfoElement = kwargs["colonSeparated"].split(";")
            self.rID = alignmentInfoElement[0]
            self.start = int(alignmentInfoElement[1])
            self.end = int(alignmentInfoElement[2])
            self.score = int(alignmentInfoElement[3])
            self.strand = alignmentInfoElement[4]
            self.width = int(alignmentInfoElement[5])
            self.gchr = alignmentInfoElement[6]
            self.gstart = int(alignmentInfoElement[7])
            self.gend = int(alignmentInfoElement[8])
            
            
        else : # initialize from keywords. need to check the kwargs dictionary properly
            self.rID = kwargs.get("rID")
            self.querySeq = kwargs.get("querySeq")
            self.gSeq = kwargs.get("gSeq")
            if "start" in kwargs:self.start = int(kwargs["start"])
            else: self.start = None

            if "end" in kwargs:self.end = int(kwargs["end"])
            else: self.end = None

            if "score" in kwargs: self.score = int(kwargs["score"])
            else: self.score = None

            if "strand" in kwargs: self.strand = kwargs["strand"]
            else: self.strand = None

            if "width" in kwargs: self.width = int(kwargs["width"])
            else: self.width = None

            if "gchr" in kwargs: self.gchr = kwargs["gchr"]
            else: self.gchr = None

            if "gstart" in kwargs:  self.gstart = int(kwargs["gstart"])
            else: self.gstart = None

            if "gend" in kwargs:  self.gend = int(kwargs["gend"])
            else: self.gend = None

            if "qSeq" in kwargs: self.qSeq = kwargs["qSeq"]
            else: self.qSeq = None
            
    def __str__(self):
        return("read = %s, query start,end,and strand = %s-%s %s , reference chrom,start,end = %s:%s-%s, score = %s, LASTmismap=%s")%(
                self.rID,self.start,self.end, self.strand,self.gchr,self.gstart, self.gend, self.score, self.LASTmismap)

    def __repr__(self):
        return self.__str__() # does not follow python standard.

    def to_colonSeparatedString(self): #read name might contain colons, so chose semicolon as separator
        return ("%s;%s;%s;%s;%s;%s;%s;%s;%s")%(self.rID, self.start, self.end, self.score, self.strand, 
        self.width, self.gchr, self.gstart, self.gend)
    
    def to_bed_feature(self):
        return("%s\t%s\t%s\t%s")%(
        self.gchr, self.gstart, self.gend+1, self.to_colonSeparatedString()) # note the self.gend+1 to be compatible with bedtools
    
    def to_groupedAlignmentOutputAdditionalField(self):
        return("(%s,%s,%s,%s)")%(
        self.gchr,self.gstart,self.score,self.LASTmismap)                                            

    def to_BitArray(self): #construct bitarray representing positions in the read, with positios in this alignment set to 1
        
        alnBitArray = self.width*bitarray('0')
        if self.strand == "+":
            for i in range(self.start,self.end+1):
                alnBitArray[i] = True
        elif self.strand == "-":
            for i in range(self.start,self.end+1):
                alnBitArray[self.width - i -1] = True
                
        return alnBitArray

    def to_MAF_feature(self):
        lal = "a score=%d mismap=%f" % (self.score, self.LASTmismap)
        lsubject = "s\t%s\t%d\t%d\t%s\t%d\t%s" % \
        (self.gchr, self.gstart, self.gend - self.gstart + 1, 
         self.gstrand, self.glen, self.gSeq)
        lread = "s\t%s\t%d\t%d\t%s\t%d\t%s" % \
        (self.rID, self.start, self.rAlnSize, 
         self.strand, self.width, self.querySeq)
#        if self.qSeq is None:
#            lqual = ""
#        else:
#            lqual = "p\t\t\t\t\t\t%s" % (self.qSeq)
        return "\n".join([lal, lsubject, lread])
        
        
class GroupAlignment(Alignment):
    """
    Subclass to account group alignments
    the rID field is the identifier  of the split graph
    all reads that were used to build the profile can be stored in the readList field
    Note: to account for split alignments this needs to be called multiple times,
    one for each aligned block
    """
    def __init__(self, **kwargs):
        if "maf" in kwargs or "colonSeparated" in kwargs:
            raise NotImplementedError("GroupAlignment objects can only be initialised from dict")
        #First the method from the parent class
        Alignment.__init__(self, **kwargs)
        #adding a new ID
        self.splitID = self.rID
        ##additional parameters
        
        if "profile" in kwargs:
            ##This should be a list of lists, of length compatible with  self.end-self.start+1
            if type(kwargs["profile"]) != type([]):
                raise TypeError("argument profile should be a list")
            self.profile = kwargs["profile"]
            if self.end-self.start+1 != len(self.profile):
                alsize = self.end-self.start+1
                psize= len(self.profile)
                #raise RuntimeWarning("looks like the profile is not of the good length : (%d, %d)" % (alsize, psize))
        else:
            self.profile = None
            
        
        #the score of the whole split 
        self.sscore = kwargs.get("sscore")
        ##This should be a list of mismap prob for all pos.
        #self.groupMismap = kwargs.get("mismap")
        #list of mismaps of the seed alignments from LAST that contribute to this GAlignment
        self.LASTMismapList = []
        self.groupMismap = None
        
        """
        if "readList" in kwargs:
            if kwargs["readList"] is not None and type(kwargs["readList"]) != type([]):
                raise TypeError("argument readlist should be a list")
            self.readList = kwargs["readList"]
        else:
            self.readList = None
        """    

        
    def __str__(self):
        l1 = Alignment.__str__(self)
        l2 = "Reads : %s, and global score: %d" % (self.readList, self.sscore)
        l3 = "ref   : %s\nquery : %s" % (self.gSeq,self.querySeq) 
        return "\n".join([l1, l2, l3])
# utility functions for processing alignments


def GroupAlignment2SAM(listGAlign):
    """
    Utility function to report a SAM line from a group of alignments corresponding to
    the same split
    """
    #test we have the same split ID
    if not all(x.splitID == listGAlign[0].splitID for x in listGAlign):
        raise StandardError("Found different splitIDs in the list")
    QNAME = str(listGAlign[0].splitID)
    FLAG = "0"
    RNAME = listGAlign[0].gchr
    RNEXT = "*"
    PNEXT = "0"
    MAPQ = "255" #No clue yet
    QUAL = "*"
    SSCORE = listGAlign[0].sscore
    
    
    
    alsorted = sorted(listGAlign, key = lambda x: x.gstart )
    SEQ = "".join((x.querySeq for x in alsorted))
    CIGAR =  str(alsorted[0].gend - alsorted[0].gstart + 1) + "M"
    TLEN = alsorted[0].gend - alsorted[0].gstart + 1
    start = alsorted[0].gstart + 1 #SAM is 1-based
    POS = str(start)
    lastend = alsorted[0].gend 
    for x in alsorted[1:]:
        msize = x.gend - x.gstart +1
        dsize = x.gstart - lastend - 1
        CIGAR = CIGAR + str(dsize) + "D" + str(msize) + "M"
        TLEN += msize
        lastend = x.gend
    
    
    #compute MAPQ, i.e. -10 log_10(groupMismap)
    
    tline = [QNAME, FLAG, RNAME, POS, MAPQ, CIGAR, 
                      RNEXT, PNEXT, str(TLEN), SEQ, QUAL, str(SSCORE)]
    #print tline
    #print map(type, tline)
    #line = "\t".join(tline)
    return tline

def filterComments(lines, isKeepCommentLines):
    for i in lines:
        if i.startswith("#"):
            if isKeepCommentLines: print i,
        else: yield i


def alignmentObjectsFromMafLines(mafLines):
    """
    takes maflines and converts them into alignment objects
    """
    for k, v in groupby(filterComments(mafLines,False), str.isspace):
        if not k: yield Alignment(maf = v)
    
            

def alignmentsFromMaf(mafLines): # lines => file handle of maf file
    """ takes in alignments from maf file which has alignments from the same read in contiguous blocks, and
    yields read id and list of alignments of this read"""
    
    alignments = alignmentObjectsFromMafLines(mafLines) # takes in maf blocks and yields Alignment objects
    
    for rid, alignments in groupby(alignments, lambda x:x.rID): # group alignments by readID 
       yield (rid, alignments)
    
    

def isCompatible(alignment1,alignment2,maxoverlap, deltabp):
    """
    do alignment1 and alignment2 look like they could be two ends of a deletion? 
    """
    assert(alignment1.start <= alignment2.start) #for now, order is important. 
    if (alignment2.gchr == "dummy"):
        return True
    
    #is there an overlap of alignments on the read side?
    olap = max(0,alignment1.end - alignment2.start+1)
    
    #print "maxOverlap , olap, delsize", maxoverlap,olap, alignment2.gstart - alignment1.gend -1 + olap
    
    if ((alignment1.strand == alignment2.strand) and 
          (alignment1.gchr == alignment2.gchr) and 
          (olap < maxoverlap) and 
          (alignment2.gstart - alignment1.gend - 1 + olap >= deltabp) 
          and (alignment2.end > alignment1.end) #added later. need to think carefully about this
          ):
        
        return True
    else:
        
        return False
 
 


def filterAlignmentsByLASTmismap(alignmentList,maxMismapProb, maxhits=50):
    
    confidentAlignments = []
    if len(alignmentList) <= maxhits:
        if maxMismapProb == 1: # do not filter
            confidentAlignments = alignmentList
        else:
            for aln in alignmentList:
                if aln.LASTmismap <= maxMismapProb:
                    confidentAlignments.append(aln)
    return confidentAlignments

#def MafFile2bedlines(mafFile, maxMismapProb,scalingFactor, maxhits,disallowFullLength):
def MafFile2bedlines(mafFile, maxMismapProb,scalingFactor, maxhits,**args):
    """ Takes in a maf file and generates lines for a bed file.
        Similar to Maf2bedlines() ,but the former takes as input a mapping dict."""
    
    #TODO better to make a separate function out of this as we are repeating the same thing we did in splitsFromMaf
    inputLines = open(mafFile, "r")
    all_maps = alignmentsFromMaf(inputLines)
    
    for rID,alignments in all_maps:
        alnsList = list(alignments)
        
        
        if args["mismapFromLAST"] == True:
            
            confidentAlignments = filterAlignmentsByLASTmismap(alnsList,maxMismapProb,maxhits)
            
        else : 
            if args["disallowFullLength"] == True:
                if any([aln.rAlnSize == aln.width for aln in alnsList]):
                    continue
            
            confidentAlignments = filterAlignments(alnsList,maxMismapProb, scalingFactor, maxhits)
        
        for confidentAlignment in confidentAlignments:
            yield confidentAlignment.to_bed_feature()
            #yield "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" %(confidentAlignment.gchr, confidentAlignment.gstart, confidentAlignment.gend+1, rID, confidentAlignment.start, confidentAlignment.end+1, confidentAlignment.width, confidentAlignment.strand, confidentAlignment.gstart, confidentAlignment.gend,"CIGAR")
    
    """
    for rID, alignments in all_maps:
        alnsList = list(alignments)
        
        if disallowFullLength:
            if any([aln.rAlnSize == aln.width for aln in alnsList]):
                continue
           
        confidentAlignments = filterAlignments(alnsList,maxMismapProb, scalingFactor, maxhits)
        for confidentAlignment in confidentAlignments:
            yield confidentAlignment.to_bed_feature()
            #yield "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" %(confidentAlignment.gchr, confidentAlignment.gstart, confidentAlignment.gend+1, rID, confidentAlignment.start, confidentAlignment.end+1, confidentAlignment.width, confidentAlignment.strand, confidentAlignment.gstart, confidentAlignment.gend,"CIGAR")
    """





def computePutativeSplits(mappings_sorted, maxoverlap, deltabp,**args): 
    """
    #TODO : there is a copy of this function in SplitGraph.py. I am not convinced that , tha is a good location for it. Since this function deals with alignments, it should be in this namespace.
    
    This function returns a list of sets of alignments. Each set in the list consists of pairs of alignments that potentially identify a (large) deletion in the reference genome. In fact, in the current implementation, a set could have more than 2 alignments if a read spans two putative deletion sites.Since we are interested only in one split at a time, we take care of multiple splits during the graph construction phase, by making each split correspond to one connected component of the graph.
    """
    # The current implementation does not consider translocations. 
    # Each set is implemented as a list, with alignments sorted according to mapping.start (ie. start point on read)value.
    # To understand the algorithm, it is worth looking at the document "Identifying splits from a set of alignments"  on trello. It is a modification of the algorithm appearing in Pal et al., "An efficient algorithm to generate all maximal independent sets on trapezoid graphs", International Journal of Computer Mathematics,  70, 587--599.
        
    #dummyMapping = mapping(mappings_sorted[0].width+1, -1, site_g.site("dummy", site_g.DONOR_SITE, -1) ,-1,'+', -1,"dummy",-1,-1,"H") # this will sit at the root of a tree we build later.
    dummyMapping = Alignment(rID="dummy",start = mappings_sorted[0].width+1, end = -1, 
                             score = -1, strand = "+", width = -1, gchr = "dummy", gstart = -1 , gend = -1 , cigar="H")    
                                                          
    mappings_sorted = mappings_sorted+[dummyMapping]
    #print "mappings_sorted",mappings_sorted
    # From here on, we will start refering to the alignments in mappings_sorted by their index number in that list. This makes things cleaner. Since the calling function requires alignments instead of their indexes, we take care of it in the return statement.
        
    children = {} # key = mapping, value = list of mappings which will be its children.Corresponds to $D_i$ in the text.
    for i,alignment in enumerate(mappings_sorted):
        children[i] = []
        for j,anotherAlignment in reversed(list(enumerate(mappings_sorted[0:i]))):
            if isCompatible(anotherAlignment,alignment,maxoverlap, deltabp): 
                if not children[i]:
                    children[i].append(j)
                else:
                    for knownChild in [mappings_sorted[knownChildIndex] for knownChildIndex in children[i]]:
                        compatibleWithKnownChild = False
                        if isCompatible(anotherAlignment,knownChild,maxoverlap, deltabp): 
                            compatibleWithKnownChild = True
                            break
                    if compatibleWithKnownChild == False:
                        children[i].append(j)
 
 
    generatorTree = {} 
    # This is a tree whose path from a leaf to the root corresponds to a set of alignments that identify a potential split.
    # Since only bottom-up traversals are required, the tree is implemented as a list of leaves (leavesList) and a dict (generatorTree) with key = nodeID, value = nodeID of parent+plus other info.
    leaves = []
    node = namedtuple('node','alignmentID,parent,depth') # add more attributes like score, percentage of read covered...
    # i am a big, fat tree, please  prune me.
    nodeID = 0
    generatorTree[nodeID] = node(len(children)-1,None,0) # add dummy as root
    currentLeaves = [0]  # a stack for depth-first tree construction
    while currentLeaves: 
        leafInFocus = currentLeaves.pop()
        labelOfLeafInFocus = generatorTree[leafInFocus].alignmentID
        if not children[labelOfLeafInFocus] :
            leaves.append(leafInFocus)
        else: 
            for child in children[labelOfLeafInFocus]:
                nodeID += 1
                generatorTree[nodeID] = node(child,leafInFocus,generatorTree[leafInFocus].depth+1)
                currentLeaves.append(nodeID)
    
    
    # traversing all leaf-to-root paths. we should choose only interesting leaves from leavesList.        
    splits = [] # list of (list of alignments identifying a split or multiple splits). 
    for i,leaf in enumerate(leaves): # go up the path from leaf to root
        splits.append([])
        nodeID = leaf
        while nodeID != 0 :
            splits[i].append(generatorTree[nodeID].alignmentID)
            nodeID = generatorTree[nodeID].parent
    #~ pprint(splits)

    
    #splits = Utils.traverseTree(generatorTree,leavesList) 
    # 
    mappings_sorted.pop() # remove that dummyMapping we added earlier
    
    return [[mappings_sorted[i] for i in j] for j in filter(lambda x: len(x)>1 ,splits)]  # returns the alignments instead of their indexes. filters out singletons. need to do more filtering.
   
   
def overlapOnReference(aln1,aln2):
    """
    Checks if two input alignments overlap on the reference side. alns must be in order
    """
    if aln1.gchr != aln2.gchr:
        return 0
    if aln1.gstart <= aln2.gstart:
        overlap = aln1.gend - aln2.gstart +1
        if overlap > 0 : return overlap
        else: return 0
    
    overlap = aln2.gend - aln1.gstart + 1
    if overlap > 0 : return overlap
    else: return 0
    
    
def overlapOnQuery(aln1,aln2):
    """
    Checks if two input alignments overlap on the query(read) side. 
    alns must be in order!!!
    """
    return(aln1.end - aln2.start + 1)


if __name__ == '__main__':

    MafFile = "tests/simpleVenter/alns.maf"
    inputLines = open(MafFile, "r")
      
    all_maps = alignmentsFromMaf(inputLines)
    for rid, alignments in all_maps:
        print rid
        print list(alignments)   
   















