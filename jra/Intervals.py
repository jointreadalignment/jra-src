from banyan import SortedSet, OverlappingIntervalsUpdator
from Alignments import Alignment



class InvalidIntervalOperationError(Exception): 
    pass
    
class Interval():
    """
    data structure to represent a genomic interval. 
    """
    
    def __init__(self, chrom, begin , end, splitID=None):
        
        #check data type and begin, end consistency
        self.chrom = chrom
        self.begin = begin
        self.end = end
        self.splitID = splitID
        
    @classmethod
    def fromAlnObject(thisClass,alnObject): #instatiates from referenence genome interval of given alignment
        chrom = alnObject.gchr
        begin = alnObject.gstart
        end = alnObject.gend
        if hasattr(alnObject,"splitID"):
            splitID = alnObject.splitID 
        else:
            splitID = None 
        return thisClass(chrom,begin,end,splitID)
   
   
    def __hash__(self):
        
        return hash((self.chrom,self.begin,self.end))  
        
    def __eq__(self,other):
        
        return other and self.chrom == other.chrom and self.begin == other.begin and self.end == other.end
    
    def overlaps(self,other):
        """
        two intervals overlap/intersect. make sure self, other ordered.
        """
        if self.chrom != other.chrom :
            raise InvalidIntervalOperatorError
        
        return (other.end >= self.begin and other.begin <= self.end)
       
    def merge(self,other): 
        """
        merge two overlapping intervals. 
        """
        if not other:
            return self
            
        elif self.chrom != other.chrom or not self.overlaps(other) :
            raise InvalidIntervalOperationError()
        
        else: return(Interval(self.chrom,min(self.begin,other.begin),max(self.end,other.end),self.splitID))
        
    
    
    def __str__ (self):
        
        return ("(%s,%s,%s,%s)")%(self.chrom,self.begin,self.end,self.splitID)



class IntervalTree(SortedSet,OverlappingIntervalsUpdator):
    """
    interfaces with banyan. modify this to interface with some other implementation
    """
    
    def __init__(self):
        
        super(self.__class__,self).__init__(key_type = (int,int), updator = OverlappingIntervalsUpdator)
        self.interval2SplitID = {} #maps interval to splitID . banyan does not allow key:data, so we have to carry this separately
        
    def addInterval(self, intervalObject):
        
        #check data type
        super(self.__class__,self).add((intervalObject.begin,intervalObject.end))
        self.interval2SplitID.update({(intervalObject.begin,intervalObject.end):intervalObject.splitID})
        
    def removeInterval(self, intervalObject):
        
        #check data type
        super(self.__class__,self).remove((intervalObject.begin,intervalObject.end))
        del self.interval2SplitID[(intervalObject.begin,intervalObject.end)]
        
    def findOverlaps(self,intervalObject):
        
        #check data type
        listOfIntvs = sorted(super(self.__class__,self).overlap((intervalObject.begin,intervalObject.end)),key=lambda x: x[0])
        return [Interval(intervalObject.chrom,intv[0],intv[1],self.interval2SplitID[(intv[0],intv[1])])  for intv in listOfIntvs]
        
class GenomicIntervalTree():
    """
    Collection of IntervalTrees, one for each chromosome.
    
    Attributes:
    
    1. the tree itself
    2. a dictionary that maps each interval to a split ID. (Each interval will be represented in exactly 1 split object.)
    
    Methods:
    
    Add, remove, query methods decide which tree to process based on the chrom attribute of input Interval object.
    
    """
    
    def __init__(self):
        self.dictOfTrees = {}  #maps chrom to its respective tree
        
    
    def addInterval(self,intervalObject):
        #TODO type checking, coordinate checking : begin < end ?
        if intervalObject.chrom not in self.dictOfTrees.keys():
            newTree = IntervalTree()
            self.dictOfTrees.update({intervalObject.chrom:newTree})
            newTree.addInterval(intervalObject)
                    
        else:
            existingTree = self.dictOfTrees[intervalObject.chrom]
            existingTree.addInterval(intervalObject)
            #self.interval2SplitID.update({intervalObject:intervalObject.splitID})
    
    def removeInterval(self, intervalObject):
        #TODO type checking
        if intervalObject is None : return 
        self.dictOfTrees[intervalObject.chrom].removeInterval(intervalObject)
        #del self.interval2SplitID[intervalObject]
        
    def findOverlaps(self,intervalObject):
        #TODO type checking
        treeToSearch = self.dictOfTrees.get(intervalObject.chrom,None)
        if treeToSearch:
            return treeToSearch.findOverlaps(intervalObject)
        else:
            return []
            
    def displayContents (self):
        
        for treeName,tree in self.dictOfTrees.iteritems():
            print treeName
            print tree
            print "interval2SplitID"
            for k,v in tree.interval2SplitID.iteritems():
                print k,v
       
    def updateFromSingleRead(self, rid, alnsInSplits): #alnsInSplits contains the alignments of a read that are part of splits, WITHOUT DUPLICATION. so if a--b, a --c  are 2 splits, alnsInSplits = [a,b,c]
        for aln in alnsInSplits:
            intv = Interval.fromAlnObject(aln)
            mergedIntv = intv.merge(aln.overlapIntv)
            if aln.overlappingIntervals: #if aln not the first to touch this region, 
                self.removeInterval(aln.overlapIntv)
            self.addInterval(mergedIntv) #even if aln was processed earliest, merged interval will be the same as leftAln.Intv
        
    """
    def updateFromSingleRead(self,rid,putativeSplits):
        #why should we do this with splits? maybe we should just do it with set of individual 
        #alns that make up the splitSet
        for alnPair in putativeSplits:
            
            if len(alnPair) > 2 : raise NotImplementedError("handling only split pairs for now")
            aln1,aln2 = alnPair
            
            #interval from current alignment pair
            intv1 = Interval.fromAlnObject(aln1)
            intv2 = Interval.fromAlnObject(aln2)
            
            #interval that needs to be updated
            olapIntv1 = aln1.overlapIntv
            olapIntv2 = aln2.overlapIntv
            
            #update the chosen interval
            mergedIntv1 = intv1.merge(olapIntv1)
            mergedIntv2 = intv2.merge(olapIntv2)
            
            if aln1.overlappingIntervals: #if aln not the first to touch this region, 
                self.removeInterval(olapIntv1)
            self.addInterval(mergedIntv1) #even if aln was processed earliest, merged interval will be the same as leftAln.Intv
            if aln2.overlappingIntervals:
                self.removeInterval(olapIntv2)
            self.addInterval(mergedIntv2)
    """
    
if __name__ == '__main__':
    
    
    intv1 = Interval("chr1",0,100,1)
    intv2 = Interval("chr1",200,300,1)
    intv3 = Interval("chr1",0,100,0)
        
    genIntTree = GenomicIntervalTree()
    
    genIntTree.addInterval(intv1)
    genIntTree.addInterval(intv2)
    
    intv4 = intv3.merge(intv1)
    
    print intv4
    """
    
    print "querying intv3"
    intv3 = Interval.fromAlnObject(Alignment(colonSeparated = "rid;10;100;500;+;90;chr3;20;110" ))

    #overlappingIntvs = genIntTree.findOverlaps(Interval("chr1",50,150))
    overlappingIntvs = genIntTree.findOverlaps(intv3)
    for item in overlappingIntvs:
        print item
     
    """
