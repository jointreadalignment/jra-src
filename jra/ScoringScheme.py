#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Contains classes and methods for manipulating scoring scheme 
and allowing computations in proba space as well
"""
import globalNames

#import shared.maf as umaf # should NOT be taking scaling factor from last 
import math
import logging
import numpy
import pprint

logger = logging.getLogger(__name__)


class ScoringScheme:
    """
    Class for scoring scheme, include scores and a method to set transitions probabilities.
    """
    def __init__(self, data = None, **attr):
    
        self.origin = None
        self.file = None
        # Match : 2-d dictionary indexed by letters eg. Match["A"]["C"]
        ##Note: Still missing indexes for unknown character : "N"
        self.Match = dict()
        
        #for gaptype in globalNames.GAPNAMES:
        #    setattr(self, gaptype, None)
            
        self.delExist = None
        self.delExtend = None
        self.Split = None
        self.insExist = None
        self.insExtend = None
        #scaling, not obvious to set up, should be given as a argument.   
        self.scaling = None
        
        #The parameters in probability space
        self.probaMatch = dict() # needs to be a dictionary over l
        self.probaLRef = []
        self.probaLQuery = []
        
        ###The probaKernel needs to be defined in the same controled vocabulary as in SplitGraph.
        self.probaKernel = dict() 
        ##Those are two parameters needed for fix the score, see Affine pHMM doc for more infos
        self.gamma = None
        self.wD = None
 
    def getAllowedVocab(self):
        """
        Returns the list of all valid attributes in the ScoringScheme
        """
        return globalNames.ALLOWED_SCORE_DESCRIPTION_VOCAB
       
    def readScoresFromFile(self, scorefile, scaling = None, **args):
        """
        Read a score matrix from a file and update the score object accordingly
        """
        logger.info("Creating score from file %s" % (scorefile))
        self.file = scorefile
        self.origin = "Score"
        with open(scorefile,"r") as f:
            #since it's not a big file, let's slurp everything
            lines = (line.rstrip() for line in f) 
            lines = list(line.split() for line in lines if line)   
        #first construct substituion matrix
        symbolsList = lines[0]
        #print "symbolsList:",symbolsList

        #do some error checking here, based on allowedVocab ?    
        #our substitution matrix will be a dict of dict
        for symbol in symbolsList:
            assert symbol in globalNames.DNA, "Error the symbol %s is not recognized as a valid DNA letter" % (symbol)
            self.Match[symbol]={}
    
        for line in lines[1:len(symbolsList)+1] :
            for i,score in enumerate(line[1:len(symbolsList)+1]): # do some error checking on length of this line 
                self.Match[line[0]][symbolsList[i]] = float(score)
        
        #add "N" character
        for symbol in self.Match:
         
            self.Match[symbol]["N"] =  min((self.Match[symbol][x] for x in self.Match[symbol]))
            
        
        self.Match["N"] = dict([(symbol, self.Match[symbol]["N"]) for symbol in self.Match])
        
        #pprint.pprint(self.Match)
        
        #now read gap penalties
        for line in lines[len(symbolsList)+1:]:
            assert line[0] in globalNames.DELNAMES + globalNames.INSNAMES, "Error the gapType \"%s\" is not recognised" % (line[0])
            setattr(self, line[0], int(line[1])) #do some error checking

        ##Check that all gap have been read
        assert self.delExist is not None, "Error gapExist field not found"
        assert self.delExtend is not None, "Error gapExtend field not found"
        ##Check also the insertion parameters 
        assert self.insExist is not None, "Error insExist field not found"
        assert self.insExtend is not None, "Error insExtend field not found"
        ###the split parameter is not mandatory
        
        
        ###Need a scaling parameter
        #assert scaling is not None, "Error the scaling needs to be set (to compute probas)"
        #self.scaling = scaling
        #self.InitProbaScheme(**args)
        return None

    def readProbaFromFile(self, probafile, scaling = None, **args):
        """
        Reads a probability file and compute the scoring scheme associated, given scaling
        This should give the reciprocal operation as from InitProbaScheme
        We hypothesize uniform length probabilities: \gamma / w_D w_I = 1 
        The 
        """
        logger.info("Reading the parameters of the pair HMM from %s" % (probafile))
        assert scaling is not None, "Error in readProbaFromFile, scaling parameter must be given"
        self.scaling = scaling        
        self.file = probafile
        self.origin = "Probabilities"
        with open(probafile,"r") as f:
            lines = (line.rstrip() for line in f) 
            lines = list(line.split() for line in lines if line and line[0] != "#")   
        #first construct substituion matrix
        symbolsList = lines[0]
        #print "symbolsList:",symbolsList

        #do some error checking here, based on allowedVocab ?    
        #our substitution matrix will be a dict of dict
        for symbol in symbolsList:
            assert symbol in globalNames.DNA, "Error the symbol %s is not recognized as a valid DNA letter" % (symbol)
            self.probaMatch[symbol]={}
    
        for line in lines[1:len(symbolsList)+1] :
            for i,proba in enumerate(line[1:len(symbolsList)+1]): # do some error checking on length of this line 
                self.probaMatch[line[0]][symbolsList[i]] = float(proba)
        
        #now read the other parameters, also gap penalties
        for line in lines[len(symbolsList)+1:]:
            assert line[0] in self.getAllowedVocab(), "Error the parameter \"%s\" is not recognised" % (line[0])
            if line[0] in globalNames.ALIGNSTATES:
                self.probaKernel[line[0]] = {}
                ##reading the transition matrix
                for k,v in [x.split("=") for x in line[1].split(",")]:
                    self.probaKernel[line[0]][k] = float(v)
            elif line[0] in ["probaLRef", "probaLQuery"]:
                setattr(self, line[0], map(float, line[1:]))
            else:
                setattr(self, line[0], float(line[1])) #do some error checking
        ##Check that all info have been read
        for p in ["probaKernel", "probaLQuery","probaLRef", "wD", "gamma"]:
            assert getattr(self, p) is not None, "Error: %s field not initialised in file" % (p)

        ###the split parameter is not mandatory
        
        ###Need a scaling parameter
        assert scaling is not None, "Error the scaling needs to be set (to compute probas)"
        self.InitScoreScheme(**args)
        return None
        
    def setDefaultGenScores(self):
        
        """
        Hard-coded default scoring scheme:
           A  C  G  T
        A 15 -80 -80 -80
        C -80 15 -80 -80 
        G -80 -80 15 -80
        T -80 -80 -80 15

        delExist -105
        delExtend -40
        insExist -105
        insExtend -40
        Split -500
        """
        
        # Match : 2-d dictionary indexed by letters eg. Match["A"]["C"]
        ##Note: Still missing indexes for unknown character : "N"
        self.Match = dict()
        for symbol in ["A","C","G","T"]:
            assert symbol in globalNames.DNA, "Error the symbol %s is not recognized as a valid DNA letter" % (symbol)
            self.Match[symbol]={}
        self.Match["A"]["A"]=float(15)
        self.Match["A"]["C"]=float(-80)
        self.Match["A"]["G"]=float(-80)
        self.Match["A"]["T"]=float(-80)
        self.Match["C"]["A"]=float(-80)
        self.Match["C"]["C"]=float(15)
        self.Match["C"]["G"]=float(-80)
        self.Match["C"]["T"]=float(-80)
        self.Match["G"]["A"]=float(-80)
        self.Match["G"]["C"]=float(-80)
        self.Match["G"]["G"]=float(15)
        self.Match["G"]["T"]=float(-80)
        self.Match["T"]["A"]=float(-80)
        self.Match["T"]["C"]=float(-80)
        self.Match["T"]["G"]=float(-80)
        self.Match["T"]["T"]=float(15)
            
             
        #add "N" character
        for symbol in self.Match:         
            self.Match[symbol]["N"] =  min((self.Match[symbol][x] for x in self.Match[symbol]))
            
        self.Match["N"] = dict([(symbol, self.Match[symbol]["N"]) for symbol in self.Match])
                       
        self.delExist = float(-150)
        self.delExtend = float(-40)
        self.Split = float(-500)
        self.insExist = float(-150)
        self.insExtend = float(-40)
        

    
    def setDefaultSeqScores(self):
        """
         A  C  G  T
        A 15 -15 -15 -15
        C -15 15 -15 -15 
        G -15 -15 15 -15
        T -15 -15 -15 15

        delExtend -30
        delExist 0
        insExtend -30
        insExist 0
        """
        
        # Match : 2-d dictionary indexed by letters eg. Match["A"]["C"]
        ##Note: Still missing indexes for unknown character : "N"
        self.Match = dict()
        for symbol in ["A","C","G","T"]:
            assert symbol in globalNames.DNA, "Error the symbol %s is not recognized as a valid DNA letter" % (symbol)
            self.Match[symbol]={}
        self.Match["A"]["A"]=float(15)
        self.Match["A"]["C"]=float(-15)
        self.Match["A"]["G"]=float(-15)
        self.Match["A"]["T"]=float(-15)
        self.Match["C"]["A"]=float(-15)
        self.Match["C"]["C"]=float(15)
        self.Match["C"]["G"]=float(-15)
        self.Match["C"]["T"]=float(-15)
        self.Match["G"]["A"]=float(-15)
        self.Match["G"]["C"]=float(-15)
        self.Match["G"]["G"]=float(15)
        self.Match["G"]["T"]=float(-15)
        self.Match["T"]["A"]=float(-15)
        self.Match["T"]["C"]=float(-15)
        self.Match["T"]["G"]=float(-15)
        self.Match["T"]["T"]=float(15)
            
             
        #add "N" character
        for symbol in self.Match:         
            self.Match[symbol]["N"] =  min((self.Match[symbol][x] for x in self.Match[symbol]))
            
        self.Match["N"] = dict([(symbol, self.Match[symbol]["N"]) for symbol in self.Match])
                       
        self.delExist = float(-30)
        self.delExtend = float(-0)
        self.insExist = float(-30)
        self.insExtend = float(-0)
        
        
    def InitProbaScheme(self, alphaD = 1e-8): ##, alphaI = 1e-4):
        """
        Initialise a set of probabilities for a pair HMM compatible with the scoring scheme.
        We consider pairHMM for gapped local alignment as described by M. C. Frith
        We make multiple additional hypotheses for restricting the degrees of freedom when
        specifying the resulting pair HMM:
        _ Homogeneous letter probabilities (\sum_x \pi_{xy} = \phi_y) and PletterQuery(x) = PletterRef(x) \forall x
        _ Uniform length probabilities: \gamma / w_D w_I = 1
        The probability to enter a deletion or an insertion state is given as a parameter 
        for the function (default is roughly one deletion every 10kbp)
        """
        ###we can set a reasonnable value for the probability to move from M to D or I as well 
        ###as the proba to stay in the same state.
        ###
        logger.info("Initialising the set of probas associated to the scoring scheme")
        ExpScoresScaled = numpy.array([[math.exp(self.Match[x][y] / self.scaling) for y in globalNames.DNA[0:4]] for x in globalNames.DNA[0:4]])
        invExpScore = numpy.linalg.inv(ExpScoresScaled)
        ##set the probas for the letters by themselves
        self.probaLRef = self.probaLQuery = list(
                    numpy.dot(numpy.ones(numpy.size(invExpScore, 0)), invExpScore))
        for x in globalNames.DNA[0:4]:
            self.probaMatch[x] = dict()
            for y in globalNames.DNA[0:4]:
                self.probaMatch[x][y] = self.probaLRef[globalNames.oDNA[x]]*self.probaLQuery[globalNames.oDNA[y]]* ExpScoresScaled[globalNames.oDNA[x]][globalNames.oDNA[y]]
        for u in globalNames.ALIGNSTATES:
            self.probaKernel[u] = dict()
        self.probaKernel[globalNames.MATCH][globalNames.DEL] = alphaD
        self.probaKernel[globalNames.DEL][globalNames.DEL] = 1. / (1. + math.exp(self.gapExist / self.scaling)/alphaD)
        self.probaKernel[globalNames.DEL][globalNames.MATCH]= 1 - self.probaKernel[globalNames.DEL][globalNames.DEL]
        
        ##non aligned ref
        wD = math.exp(-self.gapExtend/self.scaling) * self.probaKernel[globalNames.DEL][globalNames.DEL]
        gamma = wD
        self.wD = wD
        self.gamma = gamma
        ##Verify here for the splits, shouldn't I correct for the possible lengths ?
        ## e.g. divide by all possible split lengths.
        ##also normalize according to the proba for deletions
        self.probaKernel[globalNames.MATCH][globalNames.MATCH] = 1 - self.probaKernel[globalNames.MATCH][globalNames.DEL]
        if self.Split is not None:
            self.probaKernel[globalNames.MATCH][globalNames.SPLIT] = math.exp(self.Split / self.scaling)
            self.probaKernel[globalNames.SPLIT][globalNames.MATCH] = 1.
            self.probaKernel[globalNames.MATCH][globalNames.MATCH] -= self.probaKernel[globalNames.MATCH][globalNames.SPLIT]
        return None
    
    def InitScoreScheme(self, scaling = 10):        
        """
        Initialize the scoring scheme given the set of probabilities
        """
        logging.info("Initialising the set of score from the proba model.")
        print  "******\n Warning InitScoreScheme does not compute a score for N characters and gap to gap\n*********************"
        for x in globalNames.DNA[0:4]:
            self.Match[x] = {}
            for y in globalNames.DNA[0:4]:
                self.Match[x][y] = round(scaling * math.log( self.probaMatch[x][y]/ (self.probaLQuery[globalNames.oDNA[x]] * self.probaLRef[globalNames.oDNA[y]])))
        self.gapExist = round(scaling * math.log( self.probaKernel[globalNames.MATCH][globalNames.DEL] * (1 - self.probaKernel[globalNames.DEL][globalNames.DEL]) / self.probaKernel[globalNames.DEL][globalNames.DEL] ))
        self.gapExtend = round(scaling * math.log(self.probaKernel[globalNames.DEL][globalNames.DEL]/self.wD ))
        ##XXX WARNING this is maybe not correct (also in initprobaScheme)
        if self.probaKernel[globalNames.MATCH].has_key(globalNames.SPLIT):
            self.Split = round(scaling * math.log(self.probaKernel[globalNames.MATCH][globalNames.SPLIT]))
        return None

    def __repr__(self):
        """
        Some basic output of the score properties to visualise on the console
        """
        strout = "%s init from file %s\nscaling parameter: %f\n" % (self.origin,self.file, self.scaling)
        strout += "Match values:\n%s\n" % (pprint.pformat(self.Match))
        strout += "Gap Values (open, extend):\n%d, %d\n" % (self.gapExist, self.gapExtend)
        if self.Split is not None:
            strout += "Split value:\n%d\n" % (self.Split)
        strout += "Corresponding Probabilities: \n%s\n" % (pprint.pformat(self.probaMatch))
        strout += "Probabilities on the transition kernel:\n %s\n" % (pprint.pformat(self.probaKernel))
        return strout
        
        
###Some testing of the score input/output
if __name__ == '__main__':
    score_mat = "./tests/score_matrices/scoreGenome.mat"
    proba_mat = "./tests/proba_matrices/probaGenome.mat"
    maf_file = "./tests/forward-backward/two-possible-splits-same-sequence/alns.maf"
    ScoresDictFromLAST = umaf.ParseScoreTable(maf_file)
    ScoreTest = ScoringScheme()
    ScoreT2 = ScoringScheme()
    ScoreTest.readScoresFromFile(score_mat, scaling = ScoresDictFromLAST["scale"])
    ScoreT2.readProbaFromFile(proba_mat, scaling = ScoresDictFromLAST["scale"])

