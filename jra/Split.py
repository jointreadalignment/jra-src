# encoding: utf-8

from SplitGraph import SplitGraph
import Intervals
import Alignments
from Bio import SeqIO
import globalNames
from bitarray import bitarray
import copy
import gbinomial
import math

class ProcessingSplitsError(Exception): pass
class ProfileCoordinatesError(Exception): pass
class UnalignedPortionBetweenSplit(Exception): pass

class Profile():
    
    def __init__(self):
        
        self.counts = {} # key = profile Coord, value = [#A,#C,#G,#T,#-,#N]
        self.readToIntv = { } 
        # key = rid, value = bitarray of read length, representing intv which has already 
        #contributed to profile
        
        
    def updateFromSplitAln(self,read,leftAln,rightAln,readSeq,mapL,mapR):
        
        readForward = str(readSeq)
        readRevComp = str(readSeq.reverse_complement())
        readLength = len(readForward)
        
        #if first time, initialize a bitarray
        if read not in self.readToIntv:
            self.readToIntv[read] = len(readSeq)*bitarray('0')
            
        currentStatus = self.readToIntv[read]
        #print "currentStatus  ",currentStatus
            
        """
        #get a bit array from leftAln    
        leftAlnBitArray = len(readSeq)*bitarray('0')
        for i in range(leftAln.start,leftAln.end+1):
            leftAlnBitArray[i] = True
        """        
        #get bit array from leftAln
        leftAlnBitArray = leftAln.to_BitArray()
        
        #positions requiring update
        posToUpdate = leftAlnBitArray & (leftAlnBitArray ^ currentStatus)
        #print "posToUpdateL:  ",posToUpdate
        
        #update from leftAln
        if leftAln.strand == "+":
                 
            for j,i in enumerate(range(leftAln.start, leftAln.end+1)):
                        
                if posToUpdate[i]:
                    letter = readForward[i]
                    #print "letter:",letter
                    profileCoord = mapL[(leftAln.gchr, leftAln.gstart+j)]
                    #print "profileCoord",profileCoord
                    if not self.counts.get(profileCoord,None):
                        self.counts[profileCoord] = [0,0,0,0,0,0]
                    self.counts[profileCoord][globalNames.oDNA[letter]] += 1
        
        elif leftAln.strand == "-":
            
            for j,i in enumerate(range(leftAln.start,leftAln.end+1)): 
                
                if posToUpdate[readLength-i-1]: #the bit array is based on read +ve strand
                    letter = readRevComp[i]
                    profileCoord = mapL[(leftAln.gchr,leftAln.gstart+j)]
                    if not self.counts.get(profileCoord,None):
                        self.counts[profileCoord] = [0,0,0,0,0,0]
                    self.counts[profileCoord][globalNames.oDNA[letter]] +=1
        
        
        #update bit array
        currentStatus = currentStatus | posToUpdate
        
        #next rightALn
        rightAlnBitArray = rightAln.to_BitArray()
        
        """
        rightAlnBitArray = len(readSeq)*bitarray('0')
        for i in range(rightAln.start,rightAln.end+1):
            rightAlnBitArray[i] = True
        """
        posToUpdate = rightAlnBitArray & (rightAlnBitArray ^ currentStatus)#this takes care of overlaps > 0 also
        #print "posToUpdateR:  ",posToUpdate
        
        #update from rightAln. 
        
        if rightAln.strand == "+":
            for j,i in enumerate(range(rightAln.start, rightAln.end+1)):
                if posToUpdate[i]:
                    letter = readForward[i]
                    profileCoord = mapR[(rightAln.gchr, rightAln.gstart+j)]
                    if not self.counts.get(profileCoord,None):
                        self.counts[profileCoord] = [0]*len(globalNames.DNA)
                    self.counts[profileCoord][globalNames.oDNA[letter]] += 1
                    
        elif rightAln.strand == "-" :
            for j,i in enumerate(range(rightAln.start, rightAln.end+1)):
                if posToUpdate[readLength-i-1]:
                    letter = readRevComp[i]
                    profileCoord = mapR[(rightAln.gchr, rightAln.gstart+j)]
                    if not self.counts.get(profileCoord,None):
                        self.counts[profileCoord] = [0]*len(globalNames.DNA)
                    self.counts[profileCoord][globalNames.oDNA[letter]] += 1
        
        #update bit array
        currentStatus = currentStatus | posToUpdate 
        
        
        self.readToIntv[read] = currentStatus        
        #TODO if unaligned == currentStatusFlag, fill them up
        
        #TODO if insertion due to this read, then add "-" for all other reads in self.counts
        
    def getProfile(self,profCoord):
        """
        takes a profile coordinate and returns a list [#A,#C,#G,#T,#-,#N]
        """
        return self.counts[profCoord]
        
    def getCoords(self):
        """
        return the list of coords of the profile, sorted
        """
        return sorted(self.counts.keys())

    def __str__(self):
        
        toPrint = ""
        coords = sorted(self.counts.keys())
        #print "coords",coords
        
        
        numOfDigits = len(str(max(coords)))
        digitWise = zip(*[list(str(pos).zfill(numOfDigits))  for pos in coords])
        #print digitWise
        for digit in digitWise:
            toPrint += "   "
            toPrint += ''.join(digit)
            toPrint += "\n" 
        
        for letter in globalNames.DNA:
            toPrint += letter+": "
            for pos in coords:
                toPrint += str(self.counts[pos][globalNames.oDNA[letter]])
            toPrint +="\n"
        return toPrint
        
class CoordMap(dict): 

    """
    key:(chrom,coord), value : coord in profile. 
    
    Maps a position in the reference to a position in the profile.
    we will maintain this to be a one-to-one mapping.
    
    This object is required  for manipulating coordinate system while building the graph and profile.
    
    """
    
    def __init__(self,*args,**kw):
        super(CoordMap,self).__init__(*args, **kw)
        #self.interval = intv #intv is supposed to be an instance of Intervals.Interval. check this?
    
    def add(self,key,value):
        #TODO check datatype
        
        #check preexisting key. if key exists and previous value matches, then no problem; otherwise raise error
        oldValue = self.get(key,None)
             
        if oldValue is not None and value != oldValue :
            """
            print "key=",key
            print "oldValue=",oldValue
            print "newValue=",value
            """
            raise ProfileCoordinatesError("Pos. in ref already mapped elsewhere. Map needs to be 1-to-1.")
        
        self[key] = value
        
    def shiftCoords(profileStartPos,numOfCols):
        #this method is necessary when we have to insert column(s) in the profile,
        # eg. r1,r2 are processed in that order. but r2 has an insertion in it.
        #for every value that is greater than startPos in profile coords,
        #we add numOfCols.  
        for k, v in self:
            if k[v] > startPos:
                k[v] += numOfCols
        
        
class Split():
    """
    a split object aggregates information about a potential split
    """
    def __init__(self,ID): #make ID a must for init
        
        self.ID = ID
        self.graph = SplitGraph() #outlines candidate paths in the alignment matrix
        self.reads = []  #supporting reads
        self.seedAlns = {} # dictionary with key = readID, value = list of (aln.gchr,aln.gstart, score,mismap) which are the alns of readID given by last to jra
        self.coordMapDict = {} 
        # dict of  CoordMap dictionaries, one for each genomic interval pointed to by an alignment
        #key = genomic interval, value = CoordMap
        self.maxScoringSemiGlobalAln = None #list of "blocks" (GroupAlignment objects)
        self.profile = Profile()
        
        #flags
        self.UNTOUCHED = True #first time this is being updated?
        self.FLAGGED = False  #flag to signal sth went wrong during construction. if set to false, do not proceed with alignment phase.
    
    
    def __hash__(self):
        return hash(self.ID)
        
    def __eq__(self,other):
        return self.rid == other.rid
    
   
    def addToReadList(self,read):
        self.reads.append(read)
    
    def addToSeedAlnList(self,read,alnsInSplits):
        """
        updates the alnsInSplits dictionary.
        """
        self.seedAlns.update({read:alnsInSplits})
        
    #construction related
    def updateFromSingleRead(self,read,splits,alnsInSplits,readSeq):
        """
        updates split object from the read and its alignments
        """
        for split in splits:
            if len(split) > 2 : raise NotImplementedError("Split.py:updateFromSingleRead():split with more than 3 alignments")
            leftAln,rightAln = split
            
            #print "left alignment:",leftAln
            #print "right alignment:",rightAln
            
            if Alignments.overlapOnQuery(leftAln,rightAln) < 0 : raise UnalignedPortionBetweenSplit("bpshift")
            mapL,mapR = self.updateCoordMaps(leftAln,rightAln)
            self.profile.updateFromSplitAln(read,leftAln,rightAln,readSeq,mapL,mapR)
            self.graph.updateFromSplitAln(leftAln,rightAln,mapL,mapR)
        
        self.addToReadList(read)
        self.addToSeedAlnList(read,alnsInSplits)
    
    
    def updateCoordMaps(self,leftAln,rightAln):
        
        leftAlnIntv = Intervals.Interval.fromAlnObject(leftAln)
        rightAlnIntv = Intervals.Interval.fromAlnObject(rightAln)
        
        #get left coord map
        if leftAln.coordMap is not None: #this alignment is part of a different split which has been processed earlier
            
            mapL = copy.deepcopy(leftAln.coordMap)
        
        else:
            
            if not leftAln.overlappingIntervals:  
                mapL =  CoordMap()
                for j,i in enumerate(range(leftAln.gstart, leftAln.gend+1)):
                    mapL.add(key = (leftAln.gchr,i), value = leftAln.start + j)
                leftAln.overlapIntv = leftAlnIntv #allows unified downstream computation 
            
            else:
                try:
                    mapL = copy.deepcopy(self.coordMapDict[leftAln.overlapIntv])
                except:
                    raise ProcessingSplitsError("split.py:updateCoordMaps. problem with leftAln and coordMapDict")
            
        
        
        #get right coord map
        if rightAln.coordMap is not None: #this alignment is part of a different split which has been processed earlier
            mapR = copy.deepcopy(rightAln.coordMap)
        else:
            if not rightAln.overlappingIntervals: 
                mapR =  CoordMap()
                for j,i in enumerate(range(rightAln.gstart, rightAln.gend+1)):
                    mapR.add(key = (rightAln.gchr,i), value = rightAln.start + j)
                rightAln.overlapIntv = rightAlnIntv #allows unified downstream computation
           
            else:
                try:
                    mapR = copy.deepcopy(self.coordMapDict[rightAln.overlapIntv])
                except:
                    raise ProcessingSplitsError("split.py:updateCoordMaps. problem with rightAln and coordMapDict")
                
       
        
        ###now with mapL and mapR , check for consistencies
        
        ##is there is an indel next to the split?
        
        #  |-----intervalL-----|               |--------intervalR---------|
        #       ----leftAln----------|  |---------rightAln---------
        #                      <----->   <---->
        #                      offsetL    offtseR
        
        #read-side
        gapInRead = rightAln.start-leftAln.end - 1
        
        #profile-side
        offsetL = leftAln.gend - leftAln.overlapIntv.end
        offsetR = rightAln.overlapIntv.begin - rightAln.gstart 
        gapInProf = mapR[(rightAln.gchr,rightAln.overlapIntv.begin)] - offsetR - mapL[(leftAln.gchr,leftAln.overlapIntv.end)] - offsetL - 1
        
        #TODO also incorporate already-known insertions in profile
        
        if gapInRead != gapInProf: #assuming there has never been an insertion in the profile
            raise NotImplementedError("Cannot update coord map because indel detected")
                   
        if gapInRead > 0 : 
            
            
            #TODO if one of the interval end matches, then just extend one of the ends.
            #else, we don't know where this chunk belongs to. 
            #so we will allow both options: it is either part of leftAln or rightAln
            leftAln.gend += gapInRead
            leftAln.end += gapInRead
            
            rightAln.gstart -= gapInRead
            rightAln.start -= gapInRead
            
        ###
        ## real updating begins here
        ###
        if leftAln.coordMap is None:
            if leftAln.gstart < leftAln.overlapIntv.begin :
                for j,i in enumerate(range(leftAln.overlapIntv.begin-1,leftAln.gstart-1,-1)):
                    mapL.add(key = (leftAln.gchr,i), value = mapL[(leftAln.gchr,leftAln.overlapIntv.begin)] - j -1)
        
            if leftAln.gend > leftAln.overlapIntv.end :
                for j,i in enumerate(range(leftAln.overlapIntv.end,leftAln.gend+1)):
                    mapL.add(key = (leftAln.gchr,i),value = mapL[(leftAln.gchr,leftAln.overlapIntv.end)] + j+1)
        
        if rightAln.coordMap is None:
            if rightAln.gend > rightAln.overlapIntv.end :
                for j,i in enumerate(range(rightAln.overlapIntv.end+1,rightAln.gend+1)):
                    mapR.add(key= (rightAln.gchr,i) , value = mapR[(rightAln.gchr,rightAln.overlapIntv.end)] + j+1)
        
            if rightAln.gstart < rightAln.overlapIntv.begin:
                for j,i in enumerate(range(rightAln.overlapIntv.begin-1, rightAln.gstart-1,-1)):
                    mapR.add(key=(rightAln.gchr,i),value=mapR[(rightAln.gchr,rightAln.overlapIntv.begin)]-j -1)
        
        #nothing went wrong, so update the coordMapDict
        if leftAln.coordMap is None:
            if not leftAln.overlappingIntervals :
                self.coordMapDict[leftAln.overlapIntv] = copy.deepcopy(mapL)
                leftAln.coordMap = self.coordMapDict[leftAln.overlapIntv]
            else:
                oldLeftIntv = leftAln.overlapIntv
                del self.coordMapDict[oldLeftIntv] 
                newLeftIntv = oldLeftIntv.merge(leftAlnIntv)            
                self.coordMapDict[newLeftIntv] = copy.deepcopy(mapL)
                leftAln.coordMap = self.coordMapDict[newLeftIntv]
        
         
        if rightAln.coordMap is None:
            if not rightAln.overlappingIntervals:
                self.coordMapDict[rightAln.overlapIntv] = copy.deepcopy(mapR)
                rightAln.coordMap = self.coordMapDict[rightAln.overlapIntv]
            else:
                oldRightIntv = rightAln.overlapIntv
                newRightIntv = oldRightIntv.merge(rightAlnIntv) 
                del self.coordMapDict[oldRightIntv] 
                self.coordMapDict[newRightIntv] = copy.deepcopy(mapR)
                rightAln.coordMap = self.coordMapDict[newRightIntv]
            
        return leftAln.coordMap,rightAln.coordMap 
         
    
    
    
    # Alignment related
        
    def maxScoringAlignment(self,genomeDict,scoresGen,scoresSeq):
        return self.graph.maxScoringAlignment(self.profile,genomeDict,scoresGen,scoresSeq) 
    
    def tracebackList(self,hsp,genomeDict):
        self.maxScoringSemiGlobalAln = self.graph.TracebackList(*hsp, splitID = self.ID, genome_dict = genomeDict)
    
    def computeGroupMismaps(self,readProp):
        """
        compute mismap values for each group alignment block. 
        output mismap is modeled as a poisson-binomial with the mismaps of individual alignments as input parameters.
        """
        jointAlnErrProb = 0.0
        for block in self.maxScoringSemiGlobalAln:
            #print block
            for read in self.reads :
                largestOverlap = 0
                mismap = None
                for aln in self.seedAlns[read]:
                    overlap =  Alignments.overlapOnReference(aln,block)
                    if overlap > largestOverlap : mismap = aln.LASTmismap
                if mismap is not None: 
                    block.LASTMismapList.append(mismap) 
                    
            #print block.LASTMismapList
            #first get the mismap values for all possible readProps (i.e. number of successes)
            block.groupMismap = gbinomial.pGenBinom(block.LASTMismapList)
            
            #next choose the one corresponding to readProp
            numOfSuccess = int(math.ceil(readProp*len(block.LASTMismapList))) #block.LASTMismapList is the num of Reads in this block
            corrsGroupMismap = block.groupMismap[numOfSuccess] #the mismap correspoding to this readProp
            if corrsGroupMismap > jointAlnErrProb : jointAlnErrProb = corrsGroupMismap
           
        self.jointAlnErrProb = jointAlnErrProb
    
    
    def addMismapValuesToMSSGA(self):
        """
        we want to use the list of indiviual LAST-reported mismap values to 
        decide whether or not we want to output the max scoring semi-global alignment
        so once we have found such a max-scoring alignment, we want to go back to each "block" and
        add to each block, the list of mismap from individual alignments in that block
        """
        for block in self.maxScoringSemiGlobalAln:
            for read in self.reads :
                largestOverlap = 0
                mismap = None
                for aln in self.seedAlns[read]:
                    overlap =  Alignments.overlapOnReference(aln,block)
                    if overlap > largestOverlap : mismap = aln.LASTmismap
                if mismap is not None: 
                    block.LASTMismapList.append(mismap)
                
        
    def outputMaxScoringSemiGlobalAlnToSAMLine(self,expanded = False):
        """
        outputs alignments in SAM-like format. 
        """
        
        SAMcomponents = Alignments.GroupAlignment2SAM(self.maxScoringSemiGlobalAln)
        MAPQ = self.jointAlnErrProb
        SAMcomponents[4] = str(MAPQ)
        
        lineToPrint = "\t".join(SAMcomponents)
        
        READLIST = ",".join((str(readID) for readID in self.reads))
        lineToPrint += "\t"+READLIST
        
        if expanded:
            
            ALNLIST = ""
            for readName,alnList in sorted(self.seedAlns.iteritems()):
                ALNLIST += str(readName)+":"
                for aln in alnList:
                    ALNLIST += aln.to_groupedAlignmentOutputAdditionalField()
                ALNLIST += ";"
            
            lineToPrint += "\t"+ALNLIST
        
            MISMAPLIST = ""
            for block in self.maxScoringSemiGlobalAln :
                MISMAPLIST += ",".join([str(mm) for mm in block.LASTMismapList])
                MISMAPLIST += ";"
            
            lineToPrint += "\t"+MISMAPLIST
        
            GMISMAP = ""
            for block in self.maxScoringSemiGlobalAln:
                GMISMAP += ",".join([str(gmm) for gmm in block.groupMismap])
                GMISMAP += ";"
            lineToPrint += "\t"+GMISMAP
        
        return lineToPrint
        
    def __str__(self):
        
        #ID
        toPrint = "ID:"+str(self.ID)
        
        #Read list
        toPrint += "\t"+"reads:"+str(self.reads)
        
        
        #Graph edges
        toPrint += "\nGraph edges:\n"
        toPrint += self.graph.__str__()
        
                
        #Profile
        toPrint += "\nProfile:\n"
        toPrint += self.profile.__str__()
        
        return toPrint
        
