from networkx import DiGraph
import networkx as nx
from Bio import Seq,SeqIO #required for obtaining ref sequence for alignment purposes.
import globalNames
import ScoringScheme
from collections import namedtuple
import Alignments
import Intervals
import numpy as np
#import matplotlib.pyplot as plt 
from copy import copy
import logging
import pprint 

class SplitGraphConstructionError(Exception): pass
class NodeInitiationError(Exception): pass

logger = logging.getLogger(__name__)

##a few constants
MATCH = globalNames.MATCH
DEL = globalNames.DEL
SPLIT = globalNames.SPLIT
INS   = globalNames.INS
INSEQ = globalNames.INSEQ

def MoveType(n1, n2):
    """
    Type of move between 2 nodes, from n1 to n2
    """
    ##TODO this should change for accounting for DEL and SPLIT at once (a DEL is more than 1bp in general)
    #print "Checking move:", n1, n2 
    if (n1.profCoord+1    == n2.profCoord 
        and n1.refCoord+1 == n2.refCoord
        and n1.refChrom   == n2.refChrom):
        return MATCH
    elif (n1.profCoord+1    == n2.profCoord 
          and n1.refCoord+1 <  n2.refCoord
          and n1.refChrom   == n2.refChrom):
        #TODO change the data structure here
        if n1.refCoord +2 == n2.refCoord:
            return DEL
        else:
            return SPLIT
    elif (n1.profCoord +1 == n2.profCoord
          and n1.refCoord == n2.refCoord
          and n1.refChrom == n2.refChrom):
        #TODO take care of INSEQ case here
          return INS
    else:
          raise NotImplementedError("This move type does not exists")


#Node = namedtuple("Node",["profCoord","refChrom","refCoord"])

class Node():
    
    def __init__(self,coordMap,refChrom,refCoord): 
        
        self.profCoord = coordMap[(refChrom,refCoord)] #check for key error
        self.refChrom = refChrom
        self.refCoord = refCoord
        
    @classmethod
    def initFromTuple(thisClass,profCoord,refChrom,refCoord): #alternative constructor
        coordMap = {(refChrom,refCoord):profCoord}
        return thisClass(coordMap,refChrom,refCoord)
        
    
    def __eq__(self,other):
        return self.refChrom == other.refChrom and self.refCoord == other.refCoord and self.profCoord == other.profCoord
    
    
    def __hash__(self):
        return hash((self.profCoord,self.refChrom,self.refCoord))
    
    def __str__(self):
        return "("+str(self.profCoord)+","+self.refChrom+","+str(self.refCoord)+")"

    def ToMatrixCell(self):
        """
        Helper function to return a 2-uple of DP matrix row, column coordinate
        """
        return (self.profCoord, (self.refChrom, self.refCoord))
        

class SplitGraph(DiGraph):
    """
    A graph representing possible  alignment paths between 
    a reference genome and the read profile. 
    """
    
    def __init__(self, data = None, **attr):
        
        DiGraph.__init__(self, data, **attr)
    
    
    #############
    #methods required for graph construction
    #############
    
    def add_node(self, n, attr_dict = None, **attr):
        
       #TODOcheck data type 
        DiGraph.add_node(self, n, attr_dict, **attr)
    
    def addDiagonalEdge(self,coordMap,chrom,start,end,attr_dict = None,**attr):
        #TODO check data type 
        #for now, add all nodes in the diagonal. this is easier to update
        for i in range(start,end):
            DiGraph.add_edge(self,Node(coordMap, chrom, i), Node(coordMap,chrom,i+1))
        
    def addSplitEdge(self,lmap,lchrom,lpos,rmap,rchrom,rpos,attr_dict = None,**attr):
        DiGraph.add_edge(self,Node(lmap,lchrom,lpos),Node(rmap,rchrom,rpos))
        
    def addDelEdge(self,u,v,attr_dict = None, **attr):
        raise NotImplementedError("deletion edge could not be added")
    
    def addInsertionEdge(self,u,v,attr_dict = None, **attr):
        raise NotImplementedError("insertion edge could not be added")
        
        
    def add_nodes_from(self, nodes, **attr):
        raise NotImplementedError # block access to same method in parent class

    def get_inmove_dict(self, node):
        """
        Returns the set of incoming edges going to node as
        a list of tuple (move, predcoord) 
        where move is one of MATCH, INS, INSEQ, DEL, SPLIT 
        and predcoord is a 2-uple (profcoord, refcoord)
        """
        inmovedict = dict((MoveType(nprev, ncur), nprev.ToMatrixCell())
                        for (nprev,ncur) in self.in_edges_iter(node) )
        return inmovedict

    def ConstructMatrixFromNodes(self, default_elem):
        """
        Constructs a dict of dict indexed on profile coord and genome coord
        for all the nodes in the graph
        """
        M = dict()
        all_keys = list(n.ToMatrixCell() for n in self.nodes_iter())
        prof_keys = set(iprof for iprof, jref in all_keys)
        for k in prof_keys: 
            M[k] = {}
        for iprof, jref in all_keys: 
            M[iprof][jref] = copy(default_elem)
            
        return M
    
      
    def updateFromSplitAln(self,leftAln,rightAln, mapL, mapR):
        
        overlapOnQuery = Alignments.overlapOnQuery(leftAln, rightAln)
        
        if overlapOnQuery == 0 : # clean split
               
            self.addDiagonalEdge(mapL,leftAln.gchr,leftAln.gstart, leftAln.gend)
            self.addDiagonalEdge(mapR,rightAln.gchr,rightAln.gstart,rightAln.gend)
            self.addSplitEdge(mapL,leftAln.gchr,leftAln.gend,mapR,rightAln.gchr,rightAln.gstart)
        
        elif overlapOnQuery > 0: #multiple breakpoints possible
            
            self.addDiagonalEdge(mapL,leftAln.gchr,leftAln.gstart, leftAln.gend)
            self.addDiagonalEdge(mapR,rightAln.gchr,rightAln.gstart,rightAln.gend)
            
            #adding split edges:
            ##need to take care of boundary cases, where the overlap begins at start of read, or ends at end of reads
            overlapRange = range(overlapOnQuery+1)
            if leftAln.start == rightAln.start: overlapRange.pop(0)
            if leftAln.end == rightAln.end: overlapRange.pop(-1)
            #print "overlapRange",overlapRange
            for i in overlapRange:
                
                self.addSplitEdge(mapL,leftAln.gchr,leftAln.gend-overlapOnQuery+i, mapR,rightAln.gchr,rightAln.gstart+i )
            
            
     
    #~ def updateFromSplitFirstTime(self,leftAln,rightAln, mapL, mapR):
        #~ 
        #~ overlapOnQuery = Alignments.overlapOnQuery(leftAln, rightAln)
        #~ 
        #~ if overlapOnQuery == 0 : # clean split
               #~ 
            #~ self.addDiagonalEdge(mapL,leftAln.gchr,leftAln.gstart, leftAln.gend)
            #~ self.addDiagonalEdge(mapR,rightAln.gchr,rightAln.gstart,rightAln.gend)
            #~ self.addSplitEdge(mapL,leftAln.gchr,leftAln.gend,mapR,rightAln.gchr,rightAln.gstart)
        #~ 
        #~ elif overlapOnQuery > 0: #multiple breakpoints possible
            #~ 
            #~ self.addDiagonalEdge(mapL,leftAln.gchr,leftAln.gstart, leftAln.gend)
            #~ self.addDiagonalEdge(mapR,rightAln.gchr,rightAln.gstart,rightAln.gend)
            #~ 
            #~ #adding split edges:
            #~ for i in range(overlapOnQuery+1):
                #~ self.addSplitEdge(mapL,leftAln.gchr,leftAln.gend-overlapOnQuery+i, mapR,rightAln.gchr,rightAln.gstart+i )
            #~ 
            #~ 
            #~ """
            #~ nodeTopLeft = Node(mapL, leftAln.gchr, leftAln.gstart)
            #~ nodesSplitStart = [Node.initFromTuple(leftAln.end - overlapOnQuery + i, leftAln.gchr, leftAln.gend - overlapOnQuery + i ) for i in range(overlapOnQuery+1)]
            #~ nodesSplitEnd = [Node.initFromTuple(rightAln.start + i, rightAln.gchr, rightAln.gstart+i) for i in range(overlapOnQuery+1)]
            #~ nodeRightBottom = Node(mapR,rightAln.gchr,rightAln.gend)
                        #~ 
            #~ #edges
            #~ #adding at the extremeties
            #~ self.add_edge(nodeTopLeft,nodesSplitStart[0])
            #~ self.add_edge(nodesSplitEnd[overlapOnQuery],nodeRightBottom)
            #~ 
            #~ #adding split edges:
            #~ for i in range(overlapOnQuery+1):
                #~ self.add_edge(nodesSplitStart[i],nodesSplitEnd[i])
            #~ 
            #~ #tying up the backbone
            #~ for i in range(overlapOnQuery):
                #~ self.add_edge(nodesSplitStart[i],nodesSplitStart[i+1])
                #~ self.add_edge(nodesSplitEnd[i],nodesSplitEnd[i+1])
            #~ 
            #~ """
            #~ 
        #~ elif overlapOnQuery < 0 : # bp-shift
            #~ raise NotImplementedError("SplitGraph.py:updateFromSplitFirstTime: overlapOnQuery < 0")
    #~ 
    
   #~ 
    #~ def updateFromSplitNoIndel(self,leftAln,rightAln, mapL,mapR):
        #~ 
        #~ self.updateFromSplitFirstTime(leftAln,rightAln, mapL,mapR)
        #~ """
        #~ overlapOnQuery = Alignments.overlapOnQuery(leftAln, rightAln)
        #~ 
        #~ if overlapOnQuery == 0: 
            #~ 
            #~ #add split edge
            #~ self.add_edge(Node(mapL,leftAln.gchr,leftAln.gend),Node(mapR,rightAln.gchr,rightAln.gstart))
              #~ 
            #~ if leftAln.gstart < leftAln.overlapIntv.begin :
                #~ self.add_edge(Node(mapL,leftAln.gchr, leftAln.gstart),Node(mapL,leftAln.gchr,leftAln.overlapIntv.begin))
            #~ 
            #~ if rightAln.gend > rightAln.overlapIntv.end:
                #~ self.add_edge(Node(mapR,rightAln.gchr,rightAln.overlapIntv.end),Node(mapR,rightAln.gchr,rightAln.gend))
        #~ 
        #~ if overlapOnQuery < 0 :
            #~ raise NotImplementedError("overlapOnQuery < 0")
            #~ 
        #~ elif overlapOnQuery > 0 :
            #~ raise NotImplementedError("overalapOnQuery > 0 in updateFromSplits")
    #~ """
    ###############   
    #methods required during alignment phase, after all reads have been processed
    ###############
    

    def maxScoringAlignment(self,profile,genome_dict,scoresGen,scoresSeq, altype = "global"):
        #profile is an object of class Profile() defined in  Split.py
        #to get the counts of A,C,G,T,-,N at column i of the profile array,
        #use  profile.getProfile(i). this returns an array [#A,#C,#G,#T,#-,#N]
        """
        the DP matrix is encoded as a 2D hash indexed on profCoord x refCoord
        Only the nodes in the graph are in this DP matrix
        Each cell of the DP matrix is a Hash as well indexed on the different 
        moves 
        - MATCH -> list of 4 elements M(alpha, i,j) alpha \in [A,C,G,T]
        - DEL ->  list of 4 elements  D(alpha, i,j) 
        - INS -> list of 4 elements I(alpha, i,j)
        - INSEQ -> 1 element I(\star, i, j) NOT IN USE FOR NOW
        - SPLIT -> list of 4 elements S(alpha,i,j) 
              ++ note that the split and del edge are in fact implying the match state on the arriving node
        """
        DPmat = self.ConstructMatrixFromNodes({}) #hash of DP values
        DirectionMat = self.ConstructMatrixFromNodes((None,None)) #hash of backtracking links

        ZeMax = (None,None) #(max score, move) over the whole matrix
        ZeGlobMax = (None, None) #(max score, move) on end nodes
        f_update_dict = { MATCH: f_update_match,
                          DEL  : f_update_deletion,
                          SPLIT: f_update_split,
                          INS  : f_update_ins,
                          INSEQ: f_update_inseq
                        }
        nsorted = nx.topological_sort(self)
        for node in nsorted:
            iprof, jref = node.ToMatrixCell()
            Cmove = None #(previous node, move)
            bestmove = None
            logging.debug("Coord: %s"%str(jref))
            if self.in_degree(node) == 0:
                ##init DPmatrix, we hypothesize only match as first node
                ##TODO, we should clarify how insertions in the beginning
                ##of the profile could be handled
                logger.debug("Got init node %s" % (str(node)))
                logger.debug("Init node %s" % (str(node)))
                DPmat[iprof][jref][MATCH] = f_update_match(profile.getProfile(iprof), 
                                            genome_dict[jref[0]][jref[1]], 
                                            scoresGen, scoresSeq, 
                                            0.)
                DirectionMat[iprof][jref]= Cmove
            else:
                ##recurrence DP
                ##note there can be multiple items for split moves
                UpdateMoves = self.get_inmove_dict(node)
                logger.debug("updating node %s for moves %s" % (str(node),
                                                                ",".join(x[0] for x in UpdateMoves)))
                #print "updating node %s for moves %s" % (str(node),
                #                                         ",".join(x[0] for x in UpdateMoves))
                for move, predcoord in UpdateMoves.iteritems():
                    f_update = f_update_dict[move]
                    logger.debug("Move:%s, jref: %s "%(move,jref))
                    maxprev  = getMaxPrevCoord(move,
                                               DPmat[predcoord[0]][predcoord[1]],
                                               scoresGen, scoresSeq ) #get the max over possible previous moves
                    ##Update the DP matrix for each move
                    DPmat[iprof][jref][move] = f_update(profile.getProfile(iprof),
                                                        genome_dict[jref[0]][jref[1]],
                                                        scoresGen, scoresSeq, maxprev)
                    tm = max(DPmat[iprof][jref][move])
                    if tm > bestmove: 
                        Cmove = (predcoord, move)
                        bestmove = tm
                DirectionMat[iprof][jref] = Cmove
                if bestmove > ZeMax[0]:
                    ZeMax = (bestmove, (iprof, jref))
                if self.out_degree(node) == 0 and bestmove > ZeGlobMax[0]:
                    ZeGlobMax =  (bestmove, (iprof, jref))
        if altype == "local":    
            return (DPmat, DirectionMat, ZeMax)
        elif altype == "global":
            return (DPmat, DirectionMat, ZeGlobMax)
        else:
            raise NotImplementedError("Alignment type not known !")

    def TracebackSequence(self, DPmat, DirectionMat, tbackstart,genome_dict, temperature = 10.8238):
        """
        Traceback the alignment for semi-global case and return the two alignment strings
        the profile proba values and the score of the whole alignment
        """
        ##get the max score position down the profile coordinate
        ref_al = []
        group_al = [] 
        consensus_al = []
        tbackpos= tbackstart[1] ##change
        alscore = max(DPmat[tbackpos[0]][tbackpos[1]][MATCH])
        logger.debug("Doing Traceback from DP matrix, starting with score: %d" % (alscore))
        ##go back the path of positions
        while DirectionMat[tbackpos[0]][tbackpos[1]] != None:
            pnode, move = DirectionMat[tbackpos[0]][tbackpos[1]] #the next direction
            DPcell = DPmat[tbackpos[0]][tbackpos[1]]
            if move in [MATCH, INS, DEL, SPLIT]:
                mx = max(DPcell[move])
                ##Now the consensus
                ##TODO not sure I should rescale by temperature ?
                lprof = [np.exp(x-mx) for x in DPcell[move]]
                sprof = sum(lprof)
                logging.debug("letter scores : %s"%lprof)
                for i,p in enumerate(lprof): lprof[i] = p/sprof
                logging.debug("letter probas : %s"%lprof)
                ilmax = lprof.index(max(lprof))
                group_al.append(lprof)
                consensus_al.append(globalNames.DNA[ilmax])
                if move == INS:
                    ref_al.append("-")
                else:
                    ref_al.append(genome_dict[tbackpos[1][0]][tbackpos[1][1]])
                if move in [DEL, SPLIT]: ##deletions or splits
                    if move == DEL:
                        raise NotImplementedError("Deletions are not taken into account well now")
                    lsplit = tbackpos[1][1]-pnode[1][1]-1 ##the length of the split, excluding boundaries
                    group_al = group_al + ['-'] * lsplit
                    consensus_al = consensus_al + ["-"] * lsplit
                    ##Coordinates are written reverse
                    seqback = list(str(genome_dict[tbackpos[1][0]][(pnode[1][1]+1):tbackpos[1][1]].seq)) 
                    seqback.reverse()                
                    ref_al= ref_al + seqback
            elif move == INSEQ:
                raise NotImplementedError("no sequence insertion for now")
            else:
                raise UserWarning("this move does not exists" )
            tbackpos = pnode
        ##we add the last match
        DPcell = DPmat[tbackpos[0]][tbackpos[1]]
        lprof = [np.exp(x) for x in DPcell[MATCH]]
        sprof = sum(lprof)
        for i,p in enumerate(lprof): 
            lprof[i] = p/sprof
        group_al.append(lprof)
        ref_al.append(genome_dict[tbackpos[1][0]][tbackpos[1][1]])
        ilmax = lprof.index(max(lprof))
        consensus_al.append(globalNames.DNA[ilmax])
        ##Need to get the alignments in the other direction now
        ref_al.reverse()
        group_al.reverse()
        consensus_al.reverse()
        return (ref_al, group_al, consensus_al, alscore)

    def TracebackList(self, DPmat, DirectionMat, tbackstart,genome_dict, splitID = None,temperature = 10.8238):
        """
        Traceback the alignment for semi-global case and returns a list
        on the set of alignments obtained from the split graph 
        (each block separated by a split is a different groupAlignment object)
        """
#        aligninfo = dict((k,None) 
#                         for k in ["rID", "start", "end", "score",
#                                   "gscore", "strand", 
#                                   "width", "gchr", "gstart", "gend", "qSeq", "gSeq"
#                                   "profile", "readList"])
        ##get the max score position down the profile coordinate
        blocks_alignment = []
        tbackpos= tbackstart[1] ##change
        profilewidth = max(DPmat.keys()) - min(DPmat.keys()) + 1
        aligninfo = { "width" : profilewidth, "rID" : splitID }
        aligninfo["profile"] = []
        aligninfo["gchr"] = tbackpos[1][0]
        aligninfo["gend"] = tbackpos[1][1]
        aligninfo["end"]  = tbackpos[0]
        #temp variables
        alscore = max(DPmat[tbackpos[0]][tbackpos[1]][MATCH])
        sscore = alscore
        consensus_al = []
        ref_al = []
        ##TODO how to get the strand ? and rID ?
        logger.debug("Doing Traceback from DP matrix, starting with score: %d" % (alscore))
        ##go back the path of positions
        while DirectionMat[tbackpos[0]][tbackpos[1]] != None:
            pnode, move = DirectionMat[tbackpos[0]][tbackpos[1]] #the next direction
            DPcell = DPmat[tbackpos[0]][tbackpos[1]]
            if move in [MATCH, INS, DEL, SPLIT]:
                mx = max(DPcell[move])
                ##TODO not sure I should rescale by temperature - ?
                lprof = [np.exp(x-mx) / temperature for x in DPcell[move]]
                sprof = sum(lprof)
                for i,p in enumerate(lprof): lprof[i] = p/sprof
                #print "letter probas :", lprof
                aligninfo["profile"].insert(0,lprof[:])
                if move == INS:
                    ref_al.append("-")
                else:
                    ref_al.append(genome_dict[tbackpos[1][0]][tbackpos[1][1]])
                ilmax = lprof.index(max(lprof))
                consensus_al.append(globalNames.DNA[ilmax])
                
                if move == DEL:
                    raise NotImplementedError("Need to do something with the DELETION!!")
                    aligninfo["profile"].insert(0, [0] * 4)
                    consensus_al.append("-")
                    ref_al.append(genome_dict[tbackpos[1][0]][tbackpos[1][1]])
                if move == SPLIT:
                    ##TODO add a match move here, because of the structure of the split 
                    aligninfo["gstart"] = tbackpos[1][1]
                    aligninfo["start"]  = tbackpos[0]
                    aligninfo["score"]  = alscore - max(DPmat[tbackpos[0]][tbackpos[1]][SPLIT])
                    aligninfo["sscore"] = sscore
                    ##TODO here add the letter for the reference match start
                    ##the same for deletion
                    
                    ref_al.reverse()
                    consensus_al.reverse()
                    aligninfo["gSeq"]   = "".join(ref_al[:])
                    aligninfo["querySeq"]   = "".join(consensus_al[:])
                    blocks_alignment.append(Alignments.GroupAlignment(**aligninfo))   
                    
                               
                    ##reinit variables here
                    aligninfo = { "width" : profilewidth, "rID" : splitID }
                    aligninfo["profile"] = []
                    aligninfo["gchr"] = pnode[1][0]
                    aligninfo["gend"] = pnode[1][1]
                    aligninfo["end"]  = pnode[0]
                    alscore = max(DPmat[pnode[0]][pnode[1]][MATCH])
                    ref_al = []
                    consensus_al = []
            elif move == INSEQ:
                raise NotImplementedError("no sequence insertion now")
            else:
                raise UserWarning("this move does not exists" )
            tbackpos = pnode
        
        ##we add the last match and thus the last alignment
        DPcell = DPmat[tbackpos[0]][tbackpos[1]]
        mx = max(DPcell[MATCH])
        lprof = [np.exp(x-mx)/temperature for x in DPcell[MATCH]]
        sprof = sum(lprof)
        for i,p in enumerate(lprof):  lprof[i] = p/sprof
        aligninfo["profile"].insert(0,lprof)
        ref_al.append(genome_dict[tbackpos[1][0]][tbackpos[1][1]])
        ilmax = lprof.index(max(lprof))
        consensus_al.append(globalNames.DNA[ilmax])
        ##Need to get the alignments in the other direction now
        ref_al.reverse()
        consensus_al.reverse()
        
        aligninfo["gSeq"] = "".join(ref_al[:])
        aligninfo["querySeq"] = "".join(consensus_al[:])
        aligninfo["gstart"] = tbackpos[1][1]
        aligninfo["start"]  = tbackpos[0]
        aligninfo["score"]  = alscore
        aligninfo["sscore"] = sscore
        blocks_alignment.append(Alignments.GroupAlignment(**aligninfo))  
        blocks_alignment.reverse()
        return blocks_alignment

        
    def alnToSAM(self,path):
        pass

    def forwardAlignment(self,genome_dict):
        pass
    
    
    ################
    ## other utility methods
    ################
    def __str__(self):
        
        toPrint = ""
        #toPrint += "Nodes: "
        #for node,data in self.nodes_iter(data=True):
        #    toPrint += node.__str__()
        #toPrint = "\nEdges: \n" 
        for node1,node2,data in self.edges_iter(data = True):
            toPrint += "( "+node1.__str__()+" , "+node2.__str__() + " ) \n"
        return toPrint
    
    
    def draw(self,filename):
        
        pass
        #coordPairs = [] #will be a list of tuples:(refCoord, profCoord)
          
        ##get coords
        #for node in self.nodes():
            #coordPairs.append([node.refCoord,node.profCoord])
       
        
        #genomeCoords,profileCoords = map(list,zip(*coordPairs))
        #profileCoords = map(lambda x: -x ,profileCoords)
        
        #"""
        ##get plot coords for genomeCoords
        #genomeCoords.sort()
        #profileCoords.sort()
        
        #genome2PlotCoords = {}
        #for i,coord in enumerate(genomeCoords):
            #genome2PlotCoords[coord] = i
            
        ##get plot coords for profilCoords
        #profile2PlotCoords = {}
        #for i,coord in enumerate(profileCoords):
            #profile2PlotCoords[coord] = -i
        
        ##prepare node array for plotting nodes
        #genomeCoords,profileCoords = zip(*coordPairs)
        #genomePlotCoords = [genome2PlotCoords[coord] for coord in genomeCoords] 
        #profilePlotCoords = [profile2PlotCoords[coord] for coord in profileCoords]
        #"""
        
        ##plot the nodes
        #fig1 = plt.figure(figsize=(70,70))
        #ax = fig1.add_subplot(111)
        #ax.scatter(genomeCoords,profileCoords,alpha=1,s = 50)
        ##ax.scatter(genomePlotCoords,profilePlotCoords,alpha=1,s = 50)
        
        ##add edges
        #for node1,node2 in self.edges_iter():
            
            #ax.plot([node1.refCoord, node2.refCoord],[-1*node1.profCoord, -1*node2.profCoord],c="b")
            ##ax.plot([genome2PlotCoords[node1.refCoord],genome2PlotCoords[node2.refCoord]],[profile2PlotCoords[node1.profCoord],profile2PlotCoords[node2.profCoord]],c="b")
        
        ##label the nodes
        #for gCoord,pCoord in zip(genomeCoords,profileCoords) :
            #s = str(pCoord)+","+str(gCoord)
            ##plt.text(genome2PlotCoords[gCoord],profile2PlotCoords[pCoord],s)
            #plt.text(gCoord,pCoord,s, fontsize=10)
        
        #plt.axis("off")
        ##plt.savefig(filename, format = "pdf")
        #plt.savefig(filename, format = 'eps', dpi=1200)
        #plt.close('all')
      
    def toSplitEdgeList(self):
        """
        for testing purposes. 
        returns a string containing a list of node pairs that constitute list edge
        """
        edgeList = ""
        for edge in self.edges():
            if edge[0].profCoord == edge[1].profCoord - 1 and edge[0].refCoord +1 < edge[1].refCoord : # if split/deletion
                #edgeList += "("
                edgeList += edge[0].__str__()
                edgeList += ":"
                edgeList += edge[1].__str__()
                #edgeList +=");"
                edgeList += ";"
        return edgeList

####
#### Set of functions for updating the DP algorithm
####

def getMaxPrevCoord(move, MatElem, scoresGen, scoresSeq):
    """
    Given a previous cell MatElem in the DP matrix and a type of 
    move, returns the max for this very move
    """
    ##First get the overall max from last move
    lastmax = max(max(x) for x in MatElem.itervalues())
    if move == MATCH:
        maxmatch = lastmax
        return maxmatch
    elif move == DEL:
        ldel = None
        maxdel = lastmax
        raise NotImplementedError("Deletion are not implemented yet (need length of del) !!")
        for m in MatElem:
            mt = max(MatElem[m]) + ldel * scoresGen.delExtend
            if m == MATCH:
                mt += max(MatElem[m]) + scoresGen.delExist 
            elif not m in [DEL, INSEQ]:
                raise NotImplementedError("Case ",str(m),"not possible with a deletion")
            maxdel = max(maxdel, mt)
            return maxdel
    elif move == SPLIT:
        maxsplit = lastmax
        for m in MatElem:
            ##only deletion or inseq next to a split ?
            if m in [DEL, INSEQ]:
                raise NotImplementedError("Case ",str(m),"not possible with a deletion")
        maxsplit = scoresGen.Split + maxsplit
        return maxsplit
    elif move == INS:
        maxins = None
        for m in MatElem:
            if m == MATCH:
                mt = max(MatElem[m]) + scoresGen.insExist
            elif m == INS:
                mt = max(MatElem[m]) + scoresGen.insExtend
            elif m == INSEQ:
                mt = MatElem[m]
            else:
                raise NotImplementedError("case "+str(m)+" not planned")
            maxins = max(maxins, mt)
            return maxins
    elif move == INSEQ:
        raise NotImplementedError("Insertion of sequences not implemented")
    else:
        raise NotImplementedError("This move does not exists!")
    


def f_update_match(counts, lref, scoresGen, scoresSeq, maxprev):
    """
    Return the update values for a match move
    """
    logging.debug("Updating match, counts:%s"%counts)
    logging.debug("reference letter: %s"%lref)
    logging.debug("previous max value:%d"%maxprev)
    Mij = [maxprev] * 4
    for ihidletter in range(4):
        Mij[ihidletter] += counts[ihidletter]*scoresGen.Match[globalNames.DNA[ihidletter]][str.upper(lref)] 
        Mij[ihidletter] += counts[globalNames.oDNA["-"]] * scoresSeq.delExtend
        for imargletter in globalNames.oDNArange:
            Mij[ihidletter] += counts[imargletter]* \
            scoresSeq.Match[globalNames.DNA[ihidletter]][globalNames.DNA[imargletter]]
    logging.debug("scores: %s"%Mij)
    return Mij

def f_update_deletion(counts, lref, scoresGen, scoresSeq,maxprev):
    """
    Update value for a deletion move
    """
    return f_update_match(counts, lref, scoresGen, scoresSeq, maxprev)
    #return maxprev
    
def f_update_split(counts, lref, scoresGen, scoresSeq, maxprev):
    """
    Update value for a split move
    """
    return f_update_match(counts, lref, scoresGen, scoresSeq, maxprev)
    #return maxprev

def f_update_ins(counts, lref, scoresGen, scoresSeq, maxprev):
    """
    Update value for an insertion move
    """
    Iij = [maxprev]* 4
    for ihidletter in range(4):
        Iij[ihidletter] += counts[globalNames.oDNA["-"]] * scoresSeq.delExtend
        for imargletter in globalNames.oDNArange:
            Iij[ihidletter] += counts[imargletter] * \
            scoresSeq.Match[globalNames.DNA[ihidletter]][globalNames.DNA[imargletter]]
    if counts[globalNames.oDNA["N"]] > 0:
        raise NotImplementedError ("Counts for character N are not scored (yet)!!")
    return Iij
    
    
def f_update_inseq(counts, lref, scoresGen, scoresSeq):
    """
    Update value for the insertion of sequences from reads
    """
    raise NotImplementedError ("insertion of sequences from reads not implemented")




   
if __name__ == '__main__':

    
    
    #b = Node(9,"chr",109)
    #c = Node(10,"chr",200)
    #d = Node(19,"chr",209)
    
    """
    G = SplitGraph()
    
    G.add_node(a)
    G.add_node(b)
    G.add_node(c)
    G.add_node(d)   

    G.add_edge(a,b)
    G.add_edge(b,c)
    G.add_edge(c,d)
    """
    
    #for node in G.nodes():
    #    print node
    
    #nx.relabel_nodes(G,mapping,copy = False)
    
    #G.draw("./sample.pdf")
