import argparse
import math

def checkPropValue(x):
    x = float(x)
    if x < 0.0 or x > 1.0:
        raise argparse.ArgumentTypeError("%r not in range (0,1]"%(x,))
    return x

parser = argparse.ArgumentParser()
parser.add_argument(dest="inputSamFile", action = "store", help="output SAM file from JRA")
parser.add_argument("-m",dest="mismapThreshold", action = "store", type = checkPropValue, help= "group alignments with mismap more than this value are discarded", default = 0.01)
parser.add_argument("-p",dest= "prop", action = "store", type = checkPropValue, help = "at least this proportion of reads are mismapped", default = 0.3)

args = parser.parse_args()

with open(args.inputSamFile,"r") as inSAM:
    for num,line in enumerate(inSAM):
        #print "processing line:",num
        if line.startswith("#"): continue
        contents = line.split("\t") #contents[-1] is the generalized-binomial cumulative distribution values
        genBinomProbs = [ map(float,x.split(",") )for x in contents[-1].split(";")[:-1]] #gives genBinomProb distribution per groupAlignment block
        
        #how many reads are there per group alignment block?
        readsPerBlock = [ len(x.split(",")) for x in contents[-2].split(";")[:-1]]
        propReadsPerBlock = [int(math.ceil(args.prop*readCount)) for readCount in readsPerBlock]
        
        #which prob are we interested in? 
        groupMismap = max ([genBinom[propReadsPerBlock[i]] for i,genBinom in enumerate(genBinomProbs) ])
        
        if groupMismap <= args.mismapThreshold :
            print line.rstrip()+"\t"+str(groupMismap)
        
        #print "lastMM",
        #print "poisson-Binom Probs",genBinomProbs
        #print "readsperblock",readsPerBlock
        #print "propReadsperblock",propReadsPerBlock
        #print "groupMismap",groupMismap
