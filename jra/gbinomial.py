# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 11:14:44 2016

A small module to take care of the generalized binomial distribution 

@author: hrichard
"""

import numpy as np
#from scipy.stats import binom

def Convolution(X,Y):
    """
    Computes the convolution of two distribution vectors X and Y
    by doing a sum over all possible values
    """
    if len(X) < len(Y):
        X,Y = Y,X
    n = len(X)
    m = len(Y)
    Z = np.zeros(n+m-1)
    if m == 2:
        Z[0] = X[0]*Y[0]
        for i in range(1,n):
            Z[i] = X[i-1]*Y[1] + X[i]*Y[0]
        Z[n] = X[n-1]*Y[1]
    else:
        ##Ugly would be better with a loop over the
        ##indexes in Z but I am to tired today
        for i in range(n):
            for j in range(m):
                Z[i+j] += X[i]*Y[j]
    return Z

##First the trivial version
def GenBinom(P):
    """
    return the vector of the generalized Binomial from a list of 
    success probabilities P
    """
    #assert len(P)>= 2, "need at least 2 probas in the list"
    assert len(P) != 0, "empty list in GenBinom"
    Ps = sorted(P, reverse = True)
    assert Ps[0] < 1 and Ps[-1] > 0, "Error in the list of probabilities, should be in [0,1]"

    if len(Ps) == 1:
        return [1-Ps[0],Ps[0]]
    
    D = Convolution(np.array([1-Ps[0], Ps[0]]), 
                    np.array([1-Ps[1], Ps[1]]))
    for p in Ps[2:]:
        D = Convolution(D, np.array([1-p, p]) )
    return D

def pGenBinom(P):
    """
    Shortcut function for computing all significance levels for the Generalized
    Binomial (as probas to go over the threshold)
    """
    D = GenBinom(P)
    return(np.cumsum(D[::-1])[::-1])
    
    
if __name__ == '__main__':
    print "Testing the generalized binomial function"
    Probs = [0.002, 0.002, 0.002]
    distr = GenBinom(Probs)
    cumDistr = pGenBinom(Probs)
    print "Probs", Probs
    print "and dist", distr
    print "and cumulative",cumDistr
    Probs = [0.002, 0.012, 0.012, 0.002, 0.001, 0.001]
    distr = GenBinom(Probs)
    cumDistr = pGenBinom(Probs)
    
    print "Probs", Probs
    print "and dist", distr
    print "and cumulative",cumDistr

    Probs = [1.36e-9, 5.7e-10, 8.74e-7, 3.07e-8, 5.55e-10, 6.59e-10]
    distr = GenBinom(Probs)
    cumDistr = pGenBinom(Probs)
    print "Probs", Probs
    print "and dist", distr
    print "and cumulative",cumDistr

    
