#alphabet 
DNA = ['A', 'C', 'G', 'T', '-', 'N']
oDNA = dict([(v,i) for i,v in enumerate(DNA)]+ [(str.lower(v),i) for i,v in enumerate(DNA)])
oDNArange = [oDNA[i] for i in ["A", "C","G", "T", "N"]]

FILETYPES = {".fa": "fasta", ".fasta": "fasta", ".fq" : "fastq", ".fastq": "fastq"}

#alignment related
MATCH = "Match"
DEL = "Gap"
SPLIT = "Split"
INS = "Insertion"
INSEQ = "InsertRead"
JUMPS = [DEL, SPLIT]
ALIGNSTATES = [MATCH] + JUMPS + [INS]
oJUMPS = dict([(v,i) for i,v in enumerate(JUMPS)])
DELNAMES = ["delExtend", "delExist", "Split"]
INSNAMES = ["insExtend", "insExist"] 


ALLOWED_SCORE_DESCRIPTION_VOCAB = set(DELNAMES + ALIGNSTATES + INSNAMES +["scaling", "probaMatch", "probaLRef", 
                    "probaLQuery", "probaLRef", "probaKernel", "gamma", "wD"])


COUNTS = "counts"
PSSM = "PSSM"
SCORE = "maxscore"
PFW = "proba_fw"
PBW = "proba_bw"
LKSCALE = "likelihood_scale"
POSTLSCALE = "posterior_log_scale"
LOGLIKE = "log_likelihood"
BACKLOGLIKE = "backward_log_likelihood"
PPOST = "proba_post"
