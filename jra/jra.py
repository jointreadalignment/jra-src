#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Entry point to JRA. Reads in input arguments, checks file existence, launches program.
"""

import jraMain
from os import system
import os.path,os
import sys
import logging
import time
import argparse
import ScoringScheme
from Bio import SeqIO

def main():
    
    parser = argparse.ArgumentParser(description="JRA: Joint Read Aligner") 
    
    ###mandatory arguments
    parser.add_argument(dest="reads", metavar="reads", help="fasta/fastq file containing reads")
    parser.add_argument(dest="genome", metavar="genome", help="fasta file of reference genome")
    parser.add_argument(dest="maf", metavar="mafFile", help="local alignments of reads to reference in MAF format")
    
    #optional arguments
    parser.add_argument("-Q",  dest="fastq", choices = [0,1],
                        help="1 if reads are in fastq format, 0 if fasta", type=int,
                        default=0
                       )
                       
    parser.add_argument("-d", dest="minDelSize",
                      default = 20, type = int,
                      help = "minimum size of deletion")
                      
    parser.add_argument("-g", dest="scoreGenomes",
                      help="scoring scheme modeling evolutionary divergence",
                      default=False)
                      #default=os.path.join(os.path.dirname(os.path.abspath(__file__)),"defaultScores","scoreGenome.mat"))
                      
    parser.add_argument("-s", dest="scoreSequencer",
                      help="scoringscheme modeling sequencer behavior",
                      default=False)
                      #default=os.path.join(os.path.dirname(os.path.abspath(__file__)),"defaultScores","scoreSequencer.mat"))
    
        
    #alignment parameter arguments
    parser.add_argument("-o", "--max-overlap", dest="maxOverlap",
                      default = 90, type = int,
                      #help = "Maximal allowed overlap between two parts of a read to consider a split"
                      help = argparse.SUPPRESS )
    #TODO make this automatic, while reflecting on  read length. 
    
    parser.add_argument("-m", dest="maxMismapProb",
                      default = 0.6, type = float,
                      help = "max mismap probability threshold for input local alignments.")
                      
    
    parser.add_argument("-e", dest="errProb",
                      default = 0.01, type = float,
                      help = "filter out joint-alignments with error probability higher than this value")
    
    parser.add_argument("--rp", "--readProp", dest="readProp",
                      default = 0.3, type = float,
                      #help = "the proportion of supporting reads, corresponding to the number of successes in the generalized binomial distribution"
                      help = argparse.SUPPRESS)
    
    
    #debugging arguments       
    parser.add_argument("-l", "--log", dest="log", choices = ["DEBUG","INFO","ERROR"],
                      #help="write log at this level. log file is written to current directory unless outputDirectory specified by the --od option", 
                      help=argparse.SUPPRESS,
                      default = False
                      )
          
    parser.add_argument("--od", "--outputDirectory", dest="outDir", 
                      #help="directory where debugging information (logging, datastructures) is written to. ",
                      help=argparse.SUPPRESS,
                      default="./out")  
                                    
                   
    
    #parse
    args = parser.parse_args()
    
    
    #make sure all files exist
    assert os.path.exists(args.reads), "Error. cannot find the read file: %s" % (args.read)
    assert os.path.exists(args.genome), "Error cannot find the genome file:%s" % (args.genome) 
    assert os.path.exists(args.maf), "Error. cannot find the alignment file: %s" % (args.maf)
    if args.scoreGenomes:
        assert os.path.exists(args.scoreGenomes),"Error. cannot find the evolutionary distance scoring matrix file: %s" % (args.scoreGenomes)
    if args.scoreSequencer:
        assert os.path.exists(args.scoreSequencer),"Error. cannot find the sequencing error scoring matrix file: %s" % (args.scoreSequencer)
    
    
    
    #prepare logger
    if args.log:
        t = time.localtime()
        timestamp = time.strftime('%b-%d-%Y_%H%M',t)
        if not os.path.exists(args.outDir): os.mkdir(args.outDir)
        #log_file = args.outDir+"/log_"+timestamp
        log_file = os.path.join(args.outDir,"log_"+timestamp)
        numericLogLevel = getattr(logging,args.log)
        logging.basicConfig(filename = log_file,level=numericLogLevel) 
    
    else:
        logging.disable(logging.CRITICAL)
    
    
        
    
    
    
    #need exception handling  here    
    jraMain.main(args.maf, args.reads, args.genome, args.scoreGenomes, args.scoreSequencer, **vars(args))
        
    
        
    #    print("An error occurred. Quitting. See log if you ran jra with -l and --od options.")
        
    logging.info("finished")


