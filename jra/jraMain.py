# encoding: utf-8


import Alignments
import Split
import SplitGraph
import ScoringScheme
import logging
import time
import os.path
import Intervals
import networkx as nx
from networkx import is_directed_acyclic_graph
from Bio import SeqIO
#import Testing
import timeit

import sys,traceback
import warnings
warnings.filterwarnings("error")

logger = logging.getLogger(__name__)


class SplitIDError(Exception): 
    def __init__(self,rid = None,conflictingSplits = None):
        #obtained an error when processing this read.
        self.rid = rid 
        #alignments of rid have problems with the split objects in this list
        self.conflictingSplits = conflictingSplits
        

class UnexpectedIntervalOverlapError(Exception):
    def __init__(self,rid=None, aln=None,msg = None):
        self.rid = rid 
        self.aln = aln
        self.msg = msg

def chooseOverlappingInterval(rid, alnsList):   
    """
    one alignment can have multiple overlapping alignments on the reference  side. 
    in principle, one alignment should only modify one interval.
    this function chooses, for each alignment, the interval to modify. 
    the chosen alignment is set as the overlapIntv attribute of the alignment object
    """         
    
    alnsSet = set(alnsList)
    
    for aln in alnsList:
        
        #none
        if len(aln.overlappingIntervals) == 0:
            alnsSet.remove(aln)
        
        #1    
        if len(aln.overlappingIntervals) == 1 :
            aln.overlapIntv = aln.overlappingIntervals[0]
            alnsSet.remove(aln)
          
    #at this point alnsSet should contain alignments with more than 2 overlappingIntv
    remainingAlns = list(alnsSet)
    if not remainingAlns : return
    
    #from here on, not very sure of what kind of instances we might encountered.
    #for now, we expect Case(1): two sides of split with overlap on reference, or
    #Case(2): two alignments on same side of split to have different starting positions but overlapping alignments (think tandem repeats, see below)
    #  -----       -----
    #    ------    -----
    # this means that all alns with more than 1 overlapping intervals must have the same set of overlapping intervals.
    # we check this and then just assign the overlapIntv in the sorted order of position, ie. first aln
    
    
    
    #check all have same no. of overlaps and same strandedness
    
    overlapGenIntvSet = set(remainingAlns[0].overlappingIntervals)
    strand = remainingAlns[0].strand
    for aln in remainingAlns:
        if overlapGenIntvSet != set(aln.overlappingIntervals):
            raise UnexpectedIntervalOverlapError(rid)
        if strand != aln.strand:
            raise UnexpectedIntervalOverlapError(rid)
         
    #check this number matches the number of alignments in remainingAlns
    if len(remainingAlns) != len(overlapGenIntvSet): raise UnexpectedIntervalOverlapError(rid)
    
    #sort the alns and assign the genomic intervals in the same order.
    overlapGenIntvList = list(overlapGenIntvSet)
    overlapGenIntvList.sort(key = lambda x:x.begin) #for now we don't care about different chromosomes.
    
    #sort the remaning alignments in order of rstart
    remainingAlns.sort(key = lambda x: x.gstart)
    
    for i,aln in enumerate(remainingAlns):
        aln.overlapIntv = overlapGenIntvList[i]


def getSplitID(rid, splitAlnsList,intervalTree,listOfSplitObjs):
    """
    returns the ID of the split object this read will update. 
    
    this requires  checking the splitIDs of overlapping intervals for consistency.
     if no splitID mismatches occur, it returns the id. else exceptions are raised.
    
     if no exceptions, we will add aln.splitID to the id of the split object we should be updating.
    """
    #TODO merge the two blocks into one clean loop
        
    #check for splitID consistency. 
    
    #are all alignments new? 
    conflictingSplits = set()
    assignNewID = not splitAlnsList[0].overlappingIntervals #if overlappingIntervals is empty, assign new ID
    for aln in splitAlnsList:
        firstIntv = not aln.overlappingIntervals
        try :
            if firstIntv != assignNewID : raise SplitIDError()
        except:
            if not firstIntv:  
                for intv in aln.overlappingIntervals:
                    conflictingSplits.add(intv.splitID)
                    listOfSplitObjs[intv.splitID].FLAGGED = True              
            elif not assignNewID:  
                for intv in splitAlnsList[0].overlappingIntervals:
                    conflictingSplits.add(intv.splitID)
                    listOfSplitObjs[intv.splitID].FLAGGED = True

    if conflictingSplits:
        raise SplitIDError(rid,conflictingSplits)
    
    if assignNewID: #all were new
        tempID = len(listOfSplitObjs) #newest splitID available is exactly list length
        logging.debug("New split %s"%tempID)
        for aln in splitAlnsList:
            aln.splitID = tempID
        return tempID
        
        
    #At this point, it must have been that that all alns have overlapping interval(s).
    #Do they have the same split ID ?
    tempID = splitAlnsList[0].overlappingIntervals[0].splitID 
    for aln in splitAlnsList:
        for intv in aln.overlappingIntervals:
            try: 
                if tempID != intv.splitID : raise SplitIDError 
            except:                
                conflictingSplits.add(intv.splitID)
                conflictingSplits.add(tempID)
                listOfSplitObjs[intv.splitID].FLAGGED = True
                listOfSplitObjs[tempID].FLAGGED = True
                continue
            tempID = intv.splitID
            
    #print "gsa.getSplitID. tempID",tempID
    if conflictingSplits:
        raise SplitIDError(rid,conflictingSplits)
        
    
    logging.debug("Updating pre-existing split %s"%tempID)
    
    #take note of the splitID by  adding it as an attribute to aln object
    for aln in splitAlnsList:
        aln.splitID = tempID
        
    return tempID

def updateFromSingleRead(rid, readSeq,alignments, intervalTree, listOfSplitObjs, 
                        maxMismapProb = 0.32,  minDelSize = 30, maxOverlap = 90, **params):
    
    """
    mafToSplit gives this function a single read and its alignments.
    
    It first filters out less reliable alignments. 
    Next, it enumerates all possible splits.
    If there is a split,  depending on what intervals on the reference the alignments correspond to, 
    this function will either construct new split object  or update an existing one.
    If split object update succeeds, it updates the intervalTree and listOfSplitObjs before returning.

    """

    logging.debug("Processing read %s"%rid)
    
    
    #filter alignments based on mismap and existence of split alignments
    confidentAlignments = Alignments.filterAlignmentsByLASTmismap(list(alignments),maxMismapProb)
    if len(confidentAlignments) <= 1 : return #nothing interesting to look at . returning empty-handed.
    
    confidentAlignments.sort(key = lambda x: x.start)
    
    #enumerating splits   mappings_sorted, maxoverlap, deltabp, minscore, **args
    putativeSplits = Alignments.computePutativeSplits(confidentAlignments, maxOverlap, minDelSize)
    if len(putativeSplits) == 0 : return #no splits. return empty-handed.
    
    #gather splits participating in the splits #can't we just use confidentAlignments instead? perhaps not
    alnsInSplits = set()
    for split in putativeSplits:
        alnsInSplits.update(split)
    alnsInSplits = list(alnsInSplits)
    
    #find overlap with existing intervals
    for aln in alnsInSplits:
        aln.overlappingIntervals = intervalTree.findOverlaps(Intervals.Interval.fromAlnObject(aln))
        #if len(aln.overlappingIntervals) == 0:
            #aln.firstIntv = True
        #else: aln.firstIntv = False
    
    #check intervals to decide if split is new or not
    currentSplitID = getSplitID(rid,alnsInSplits,intervalTree, listOfSplitObjs) 
    
    #if there was any error, getSplitID would have raised an error and control would have gone back to mafToSplitList.
    #we are here means there were no errors. so let's continue.
    #if an alignment has multiple overlapping intervals (in case of genomic overlap), decide which one to manipulate
    #also there might be certain combinations we don't want to handle. eg. left and right alignments have unequal number of overlapping intervals
    
    chooseOverlappingInterval(rid,alnsInSplits) 
    
    #update corresponding split object
    if currentSplitID == len(listOfSplitObjs): # i.e. if this split is new
        currentSplitObject = Split.Split(currentSplitID)
        
    else:
        currentSplitObject = listOfSplitObjs[currentSplitID] #pull out the existing split object
       
      
    #TODO check if the current split obj is already flagged?? if yes, then do something??
    currentSplitObject.updateFromSingleRead(rid,putativeSplits,alnsInSplits,readSeq)
    #update interval tree. placement is crucial. 
    #don't place anything between the split.updateFromSingleRead call and the following.
    #this is ensure intervals that are the keys in split.CoordMapDict are in sync with intervals in intervalTree.
    intervalTree.updateFromSingleRead(rid,alnsInSplits)
    
    #update the list of splits
    if currentSplitID == len(listOfSplitObjs):
        listOfSplitObjs.append(currentSplitObject)
       

def mafToSplitList(all_maps,readsGen, **params):
        
    """
    From a Maf file of alignments, identifies splits, construct split objects, 
    puts them in a list and returns the list.
    """
    
    listOfSplitObjs = []  #initialize list of splits. this will be updated by sub-functions.
    intervalTree = Intervals.GenomicIntervalTree() #this structure keeps track of ref genomic intervals corresponding to gapless alignments . 
        
    for rid,alignments in all_maps:
               
        #updateFromSingleRead(rid, alignments,intervalTree, listOfSplitObjs,**params)
        
        try:
            
            nextRead = readsGen.next()
            while nextRead.id != rid :
                nextRead = readsGen.next()                
            readSeq = nextRead.seq
            
            updateFromSingleRead(rid, readSeq, alignments,intervalTree, listOfSplitObjs, **params)
            
        except SplitIDError as e:
            logging.error("Split ID error with read %s . ID(s) of conflicting splits: %s"%(rid,e.conflictingSplits.__str__()))      
            continue        
        
        except UnexpectedIntervalOverlapError:
            logging.error("Unexpected multiple interval overlap error with %s"%(rid))
            continue
            
        except Split.ProcessingSplitsError as e:
            logging.error("with %s, following error occurred: \n %s"%(rid,e.args))
            continue
            
        except Split.ProfileCoordinatesError as e:
            logger.error("With %s, following error occurred: \n %s"%(rid,e.args))
            #get split id also. log this and read list. then flag the split
            continue
        
        except SplitGraph.SplitGraphConstructionError:
            logger.error("SplitGraph construction error with read %s"%(rid))
            continue
            
        except Split.ProcessingSplitsError as e :
            logger.error("With %s, following error occurred: \n %s"%(rid,e.args) )
            continue
            
        except NotImplementedError as e :
            logger.error("With %s, following error occurred: \n %s"%(rid,e.args))
            continue
        
        except StopIteration:
            logger.error("Are reads in the read file the same order as in alignment file?")
            raise
        
        except Split.UnalignedPortionBetweenSplit as e :
            logger.error("With %s, following error occurred: \n %s"%(rid,e.args))
        
        """
        except Exception as e:
            logging.error("Unexpected error with %s"%(rid))
            logging.error(e)
            continue
        """
    
        
    return listOfSplitObjs



def main(mafFile, readsFile, genomeFile, scoresGenFile, scoresSeqFile, showSplit=False, outDir="./" ,fastq=0 ,**params):
    """
    This function computes a list of split objects*, and for each split object, computes a highest scoring alignment.
    *A split object is a data structure that holds all necessary information about a potential split. See split.py for definition.
    """
   
 
    ##read in files and instantiate generators and other objects
    #reference genome
    seqformat = "fasta"
    genomeDict = SeqIO.to_dict(SeqIO.parse(genomeFile, seqformat)) ##Temp, we will need index() function for genome      
    
    #alignment file
    inputLines = open(mafFile, "r")
    all_maps = Alignments.alignmentsFromMaf(inputLines) # generator which yields all alignments of one read 
    
    #read file
    readsHandle = open(readsFile,"r")
    if fastq == 1:
        readFileType="fastq"
    elif fastq == 0:
        readFileType="fasta"
    readsGen = SeqIO.parse(readsHandle,readFileType) # we need the reads also, because some positions in the read might not be covered by its alignments
    
    #score file
    #prepare scoring matrices
    scoresGen = ScoringScheme.ScoringScheme()
    if scoresGenFile:
        scoresGen.readScoresFromFile(scoresGenFile)
    else:
        scoresGen.setDefaultGenScores()
        
    scoresSeq = ScoringScheme.ScoringScheme()
    if scoresSeqFile:
        scoresSeq.readScoresFromFile(scoresSeqFile)
    else:
        scoresSeq.setDefaultSeqScores()
    
    start_maf2Split = timeit.default_timer()
    listOfSplitObjects = mafToSplitList(all_maps, readsGen, **params) #See Split.py for definition of a split object
    elapsed_maf2Split = timeit.default_timer() - start_maf2Split
    logger.info("%s splits found"%(len(listOfSplitObjects)))
    logger.info("It took %f seconds to read in the alignments and construct the split objects"%(elapsed_maf2Split))
    #lal = []
    
    start_jointAlignment = timeit.default_timer()
    for split in listOfSplitObjects:
        
        
        
        logger.debug("Processing split %s"%(split.ID))
        
        if nx.is_directed_acyclic_graph(split.graph):
            
            if showSplit: #we want to look at the graph in case maxScoringAlignment fails.
                split.graph.draw(outDir+"/"+str(split.ID)+".eps")
            
            try:
                
                hsp = split.maxScoringAlignment(genomeDict,scoresGen,scoresSeq)                
                split.tracebackList(hsp,genomeDict)
                #split.addMismapValuesToMSSGA()]
                split.computeGroupMismaps(params["readProp"])
                if split.jointAlnErrProb < params["errProb"] :
                    print split.outputMaxScoringSemiGlobalAlnToSAMLine()
            
            except Exception as e:
                
                exc_type, exc_value, exc_traceback = sys.exc_info()
                                                
                exceptionInfo = repr(traceback.format_exception(exc_type, exc_value,
                                          exc_traceback))
                logging.error(exceptionInfo)
                continue
                
        else:
            logger.debug("Graph of split %s is not a DAG. Moving to next"%(split.ID))
            if showSplit:
                split.graph.draw(outDir+"/"+str(split.ID)+".eps")
        
    elapsed_jointAlignment = timeit.default_timer() - start_jointAlignment
    logger.info("It took %f seconds to perform joint alignment"%(elapsed_jointAlignment))
    
    inputLines.close()
    
    
    #Testing.toListOfJunctionEdges(listOfSplitObjects,"./tests/debug/list-splits.txt")
    

#if __name__ == '__main__':
    
    
