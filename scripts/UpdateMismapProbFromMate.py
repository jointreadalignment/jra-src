# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 11:47:28 2016

@author: hrichard
"""
import gzip
import re
import numpy as np
import Alignments
import argparse
import itertools
import os

###
### A small script to process the mate pair reads and add the info about 
### multimapping

########################################################################
####
####
####      SAM FILE PART
####
########################################################################

def ParseSamAsDict(samfile):
    """
    Parse a gzipped sam file and returns a dict indexed by IDs
    """
    fsam = gzip.open(samfile, 'rb')
    dmate = {}
    for l in fsam:
        tline = l.rstrip().split("\t")
        rid = tline[0]
        if rid not in dmate:
            dmate[rid] = []
        dmate[rid].append(tline)
    fsam.close()
    return dmate

def getMultiMapSam(fname):
    """
    from a gzipped sam file, 
    returns a dictionnary multimapping reads where at least one of the mapping
    has a soft mapping information
    """
    f= gzip.open(fname, 'rb')
    dreads = {}
    lselect = []
    nmultimap = 0
    i = 0
    nreads = 0
    prid = None
    crid = None
    clreads = []
    filelines = (l for l in f if l[0] != "@")
    softmap = False
    for l in filelines:
        tl = l.rstrip().split("\t")
        clreads.append(l.rstrip())
        crid, cigar= tl[0],tl[5]
        softmap = softmap or cigar.find("H")>=0
        nreads += 1
        if crid != prid :
            if nreads > 1 and softmap :
                ##TODO take care of splitting the lines
                dreads[crid] = clreads[:]
                lselect.append(crid)
                nmultimap += 1                
            clreads = []
            nreads = 0
            softmap = False
            i += 1
            if i % 500000 == 0:
                print "Read n.", i , "nmultimap:", nmultimap
        prid = crid
    f.close()
    print "found a total of", nmultimap, "multimapping reads with softmap"
    print "size of the dict", len(dreads)
    return (dreads, lselect)


def GetReadsFromListSAM(fname, lreads, outfile):
    """
    Gets the reads in fname that occur in lreads and write it to outfile
    """
    sreads = set(lreads)
    f= gzip.open(fname, 'rb')
    out = gzip.open(outfile, 'wb')
    i = 0 
    for l in f:
        rid = l.split("\t")[0]
        if rid in sreads:
            out.write(l)
            i += 1
            if i % 1000 == 0:
                print "added read n.", i
    f.close()
    out.close()

def parseCigarString(cigar):
    """
    Parse a cigar string and return two lists with
    - the types of moves (of M, I, D, H)
    - the number of such letters
    """
    #skipcar = set(['D'])
    car1 = set(["M", "I", "=", "X"])
    car0 = set(["S", "H"])
    moves = np.array([])
    match = re.findall(r'(\d+)(\w)', cigar)
    Mlen = []
    for nlet, al in match:
        if al in car1:
            moves = np.append(moves, np.ones(int(nlet)))
            Mlen.append(int(nlet))
        elif al in car0 :
            moves = np.append(moves, np.zeros(int(nlet)))           
    return (moves, max(Mlen))
        

def extractMateInfoSAM(dmate, rid, losoft):
    """
    return some summary stats on the mate
    - nmates
    - min distance to the set of candidates
    - mapping quality
    - nmatching positions
    if no mate mapped, return -1
    """
    if rid not in dmate:
        return ("0", "-1", "-1", "-1")
    lomate = dmate[rid]
    matepos = np.array([int(t[3]) for t in lomate])
    softpos = np.array([int(t[3]) for t in losoft])
    matedist = np.array([np.min(np.abs(softpos - mp)) for mp in matepos])
    bestmate = np.argmin(matedist)
    nmatch = np.sum(parseCigarString(lomate[bestmate][5])[0])
    return (len(lomate), matedist[bestmate], 
            lomate[bestmate][4], nmatch)


def isACandidateSplit(loreads, mincperc = 0.5, lread = 100):
    """
    From a list of reads alignment lists with the same ID and soft clipped, decides wether 
    they make a candidate split
    - same chromosome
    - at least xx% is aligned somewhere
    - the orientations of alignment are compatible
    returns the status of the list of reads with descriptive summaries
    """
    nreads = len(loreads)
    cover = np.zeros(lread)
    chrs = [None] * nreads
    orient = np.zeros(nreads)
    mapqual = [None] * nreads
    anchorlen = np.zeros(nreads)
    for i,tline in enumerate(loreads):
        cigar = tline[5]
        ccov, Ml = parseCigarString(cigar)
        cover += ccov
        anchorlen[i] = Ml
        orient[i] = int(tline[1]) & 16
        chrs[i] = tline[2]
        mapqual[i] = tline[4]
    perc_cover = np.mean(cover>0)
    sameorientation = np.all(orient == 0) or np.all(orient > 0)
    passtest = int(sameorientation and perc_cover >= mincperc and len(set(chrs)) == 1)
    quals = "\""+":".join(mapqual) + "\""
    worstqual = np.min(map(int, mapqual))
    minanchor = np.min(anchorlen)
    return (rid, nreads, passtest, perc_cover, sameorientation, 
            minanchor, quals, worstqual)



########################################################################
####
####
####                    MAF FILE PART
####
########################################################################


def getMultiMapMAF(mafFile):
    """
    From a gzipped maf file
    returns a dictionnary of the multimapping reads where at least one of the
    mapping is not mapping on its full length
    """
    inputLines = gzip.open(mafFile, "rb")
    all_maps = Alignments.alignmentsFromMaf(inputLines) # generator which yields all alignments of one 
    dal = {}
    lselect = []
    nparsed = {'reads' : 0, 'multimap' : 0}
    for rid, alignments in all_maps:
    	##FIXME the name of mate 1 and 2 are hardcoded to 1/ and 2/
    	ridtrim = rid.rstrip('12').rstrip('/')
        ##check the number alignments
        la = list(alignments)
        nparsed['reads'] += 1
        if nparsed['reads'] % 1000000 == 0 :
            print "parsed %d Mio reads" % (nparsed['reads']/1000000)
        if len(la) > 1 :
            largestsoftclip = max(itertools.imap(lambda x: x.width - x.rAlnSize, la))
            if largestsoftclip > 5:
                dal[ridtrim] = la
                lselect.append(ridtrim)
                nparsed['multimap'] += 1
                if nparsed['multimap'] % 50000 == 0 :
                    print "and %d K multimapped reads" % (nparsed['multimap']/1000) 
    inputLines.close()
    return (dal,lselect)




def ParseMAFAsDict(mafFile, lfilter = None):
    """
    Parse the maf file as a dict of rid 
    when lfilter is provided, only the reads with rid in the list are returned
    """
    sfilt = set(lfilter) #we prefer to have a set 
    ##alignment file
    inputLines = gzip.open(mafFile, "rb")
    all_maps = Alignments.alignmentsFromMaf(inputLines) # generator which yields all alignments of one read 
    ##
    all_maps_trim = itertools.imap(lambda (x,y) : (x.rstrip('12').rstrip('/'), y), all_maps)
    if lfilter is not None:
        ireads = itertools.ifilter(lambda (x,y): x in sfilt, all_maps_trim)
    else :
        ireads = all_maps_trim
    return dict(itertools.imap(lambda (x,y): (x, list(y)), ireads))
    

def distAlToInsert(al):
    """
    return the distance to the insert for an alignment
    if pos strand : al.start
       neg strand : al.width - al.end - 1
    """
    if al.strand == "-" : 
        return al.start
    else:
        return al.width - al.end -1

def InsertSize(alm1, alm2, bperror = 2):
    """
    Computes the insert size between alm1 and alm2, the number of
    bp within the interval.
    the two alignments have to spread up to the side of the read next to the insert, for instance an
    alignment of only the 50 first bp of a 100 bp read would not be taken into account 
    because it could be one side of a split.
    We allow at most bperror bp of distance
    """
    if alm1.strand != alm2.strand and alm1.gchr == alm2.gchr and distAlToInsert(alm1) <= bperror and distAlToInsert(alm2) <= bperror:
        if alm2.strand == "+" and alm1.strand == "-":
            dist = alm1.gstart - alm2.gend - 1
        elif alm1.strand == "+" and alm2.strand == "-" :
            dist = alm2.gstart - alm1.gend - 1
        else:
            dist = -1
        return dist
    else:
        return -1 

#
#def getInsertSizeDist(mafFilem1, mafFilem2, nals = 10000):
#    """
#    Helper function to get the distance distribution between fm1 and fm2
#    
#    """
#    
#    inputLines1 = gzip.open(mafFilem1, "rb")
#    all_maps_m1 = Alignments.alignmentsFomMaf(inputLines1)
#    inputLines2 = gzip.open(mafFilem1, "rb")
#    all_maps_m2 = Alignments.alignmentsFomMaf(inputLines2)
#    IS = [-1] * nals
#    i = 0
#    while i < nals :
#        rid1, als1 = all_maps_m1.next()
#        la1 = list(als1)
#        r2, als2 = all_maps_m2.next()
#        la2 = list(als2)
#
#        IS.append(InsertSize(la1[0], la2[0])
#    return(IS)

def updateMismapFromMateInfoMAF(rid, dm1, dm2, drange):
    """
    Extract relevant mapping information from the list of mates of 
    a read rid with alignments alignments, in order to decide later if the mismap 
    proba can be transferred
    By default orientation is +/- on the two mates
    """
    ###first the array of all als to als distances
    #print "testing read", rid
    updated = 0
    D = np.array(map(lambda x:InsertSize(*x), itertools.product(dm1[rid], dm2[rid])))
    nm1als = len(dm1[rid])
    nm2als = len(dm2[rid])
    D = np.reshape(D, (nm1als, nm2als))
    mwithinrange = np.logical_and(D >= drange[0], D <= drange[1])
    m1withpair = np.any(mwithinrange, axis = 1)
#    print "distances"
#    print D
#    print "*The mates 1\n"
#    for alm1 in dm1[rid]: print "---",alm1,"\n" 
#    print "*The matepairs\n" 
#    for alm2 in dm2[rid]: print "---",alm2,"\n"  
    if sum(m1withpair) > 0:
        nrescuecandidates = np.sum(mwithinrange[m1withpair,], axis = 0)
#        print "****\nChecking to update read %s with %s candidates" % (rid, str(nrescuecandidates))
        for im1 in np.where(m1withpair)[0]:
            ##we propose to rescue with the lowest mismap proba that 
            ##has not an other candidate in the list
            m1mismap = dm1[rid][im1].LASTmismap
            im2 = np.where(mwithinrange[im1,])[0]
            m2mismaps = [(i, dm2[rid][i].LASTmismap) for i in im2 if nrescuecandidates[i] < 2]
            m2mismaps = sorted(m2mismaps, key = lambda x: x[1])
#            print "* mismap of mate 1:%e, mismaps of mates2:%s" % (m1mismap, str(m2mismaps))
            if len(m2mismaps) > 0 and m2mismaps[0][1] <= m1mismap:
                updated = 1
                if m2mismaps[0][1] == m1mismap:
                    updated = 2
#                print "+++ updating %s with new proba %e" % (rid, m2mismaps[0][1])
                dm1[rid][im1].LASTmismap = m2mismaps[0][1]
    ###get the alignments that are within the distance range given by drange
    return updated 
    
def rescueMismapProbas(dm1, dm2, drange = [50, 124]):
    nup = [0,0,0]
    rset1 = set(dm1.keys())
    rset2 = set(dm2.keys())
    update_set = rset1.intersection(rset2)
    for rid in update_set:
        isup = updateMismapFromMateInfoMAF(rid, dm1, dm2 , drange)
        nup[isup] += 1
    return nup


Usage = "Usage: %prog -1/--mate1 <mate_forward> -2/--mate2 <mate_reverse> --fmean <fragment mean size> --fsd <fragment sd size> -z/--zscore <z gaussian factor> -o/--output <output_file>"
description = """
A short script to update the mapping quality information using the indication from the mate
and estimates on the fragment insert size distribution.
The first and second mates are then written to a additional MAF file

The reads are first filtered for containing at least two mapping positions on the reference and 
indexed 

The two mate pairs are supposed to be mapping in reverse orientation 
"""
parser = argparse.ArgumentParser(description=description)
                                 
parser.add_argument("-1",  "--mate1", dest = "fmate1", 
                    help="MAF alignment file for mate1")
parser.add_argument("-2",  "--mate2", dest = "fmate2", 
                    help="MAF alignment file for mate2")

parser.add_argument("-o", "--out", dest = "fout",
                    help = "output file name for mate 1")

parser.add_argument("--fmean", dest = "fragmentmean",
                    type = float, default = 288,
                    help = "mean of the insert size distribution (between position of the 2 mates)")

parser.add_argument("--readlen", dest = "readlen",
					type = float, default = 100,
					help = "length of the reads")

parser.add_argument("--fsd", dest = "fragmentstd", 
                    type = float, default = 28,
                    help = "standard deviation of the insert size distribution")

parser.add_argument("-a", "--alpha", dest = "alpha",
                    type = float, default = 1.28,
                    help = "z factor to consider a read to be within the distance of the insert size")



#parse
args = parser.parse_args()

assert os.path.exists(args.fmate1), "Error. cannot find the read file for mate1: %s" % (args.fmate1)
assert os.path.exists(args.fmate2), "Error. cannot find the read file for mate2: %s" % (args.fmate2)

insert_size = args.fragmentmean - 2*args.readlen
size_range = [insert_size - args.alpha * args.fragmentstd, insert_size + args.alpha * args.fragmentstd]

##Step 1 GetMultimap reads from the maf file
print "Parsing mate 1 file\n"
dmate1, lsel = getMultiMapMAF(args.fmate1)
print "Parsed a total of %d reads with multimapping" % (len(dmate1))

print "Parsing mate 2 file for matching\n"
##Step 2 Get a matching dictionnary from the complementary matepair file

dmate2 = ParseMAFAsDict(args.fmate2, set(dmate1.keys()))
print "Parsing the mate pair, total of %d reads" % (len(dmate2))

##Step 3 Go through all the reads and check for a compatible pair
##Update the score 
print "rescuing mismap probabilities on first mate according to the info on 2nd mate..."
lresc = rescueMismapProbas(dmate1,dmate2, drange = size_range)
print "Rescue summary (not rescued, rescued, same mismap proba): %s\ntotal of %d mate1" % (str(lresc), len(dmate1)) 

out = gzip.open(args.fout+".gz", "wb")

for rid in lsel:
    for al in dmate1[rid]:
        out.write(al.to_MAF_feature()+"\n\n")

out.close()



