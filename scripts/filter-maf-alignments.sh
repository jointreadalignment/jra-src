#! /bin/sh

if [ $# -ne 2 ]; then
    echo "please provide a maf file and a max mismap probability threshold"
    echo "usage:" $0 "mafFile" "mismap"
    exit 1
fi

mafFile=$1
maxMismap=$2

grep -v ^# ${mafFile} |  
awk -v mm=$maxMismap '
BEGIN{RS=""; BEGIN_BLOCK = 1; prevRID = -1} 
{ 
    if (substr($3,8)+0 <= mm)
    {
        if ($12 != prevRID) 
        {
            if (BEGIN_BLOCK != 1) {print prevLine"\n"};
            prevRID=$12;
            prevLine=$0;
            BEGIN_BLOCK = 1;
        }
        else
        {
            print prevLine"\n";
            prevLine=$0;
            BEGIN_BLOCK = 0;
        }
    }
}
END{if (BEGIN_BLOCK == 0) {print prevLine};}
'
