#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
contains classes and methods which might be required for testing purposes
"""
"""
import Split
import SplitGraph
import logging
"""
import matplotlib.pyplot as plt
import numpy as np


def toListOfJunctionEdges(listOfSplitObjs,fileout):
    """
    writes out a file with three columns, first containing graph id and second a list of its
    junction edges, and third containing the reads that support the edges. 
    """
    f = open(fileout,"w")
    
    for split in listOfSplitObjs:
            
        lineToOutput = ""
        lineToOutput += str(split.ID)+"\t"
        lineToOutput += split.graph.toSplitEdgeList()+"\t"
        lineToOutput += ";".join([str(readName) for readName in split.reads])
        lineToOutput += "\n"
        f.write(lineToOutput)    
        
    f.close()
        
def computeReadSupportHistogram(listOfSplitObjs,fileout):
    countArray = [len(split.reads) for split in listOfSplitObjs]
    maxSupport = max(countArray)
    bins = np.arange(maxSupport+1)[1:]
    hits,edges = np.histogram(countArray,bins=bins)
    
    fig,ax = plt.subplots()
    ax.bar(edges[:-1],hits)
    tickPos = [ b+1/2.  for b in edges[:-1] ]
    ax.set_xticks(tickPos)
    ax.set_xticklabels(edges[:-1])
    ax.set_yscale('log')
    
    plt.savefig(fileout,format = "png")
