import Alignments
import logging
from intervaltree import Interval, IntervalTree
import time
import os.path

class MatrixSplitGraph():

    def SplitsFromMaf(self, MafFile, maxMismapProb = 1, scalingFactor = 10.8238, maxhits = 50,  **args):
        """
        From a Maf file of alignments, identifies split alignments that indicate deletion event(s), 
        and constructs a graph that represents the deletions.
        All reads with more than maxhits or alignments with mismap probability more than maxMismapProb are ignored
        (should take care of that in order to mask non interesting portions of the genome).
        ScalingFactor is required to calculate the mismap probability unless masMismapProb = 1, in which case the filter
        function passes everything through.
        **args is a dictionnary of argument passed to the method UpdateFromReadMaps
        (for instance maxoverlap or  )        
        returns a link to the dictionnary of read alignments (indexed by the alignments)
        """
        
       
        
        assert os.path.exists(MafFile), "Error %s does not exists"
        inputLines = open(MafFile, "r")
            
        #For each read, get all its alignments. 
        all_maps = Alignments.alignmentsFromMaf(inputLines)
        
        intervalTree =   IntervalTree()
        #intervalToReads = {} 
        
        if args["mismapFromLAST"] == True: # putting this inside for loop would make prettier code, but would have to check every time. 
            
            for rid,alignments in all_maps:
                logging.debug("Processing read %s"%rid)
                confidentAlignments = Alignments.filterAlignmentsByLASTmismap(list(alignments),maxMismapProb,maxhits)
                if len(confidentAlignments) > 1 : # the else block has the same lines. change filterAlignmentsxx to a generator?
                    confidentAlignments.sort(key = lambda x: x.start)
                    putativeSplits = Alignments.computePutativeSplits(confidentAlignments,**args)
                    for splitAlns in putativeSplits:
                        assert len(splitAlns) == 2, "not implemented for multiple splits"
                        self.addSplitToGraph(rid,splitAlns,intervalTree)
                        
        else:
            assert False,"not implemented"
            """
            for rid, alignments in all_maps:
                confidentAlignments = Alignments.filterAlignments(list(alignments),maxMismapProb, scalingFactor, maxhits)
                if len(confidentAlignments) > 1 :
                    self.UpdateFromReadMaps(rid,confidentAlignments,**args)
            """    
        inputLines.close()
        
        
    
    def addSplitToGraph(self,rid,splitAlns, intervalTree_b):
        
        
        aln1,aln2 = splitAlns
        
        
        
        
        intervals1 = sorted(intervalTree_b[aln1.gstart:aln1.gend])
        intervals2 = sorted(intervalTree_b[aln2.gstart:aln2.gend])
        
        
        #for iv in intervals1:
        #    if "512786" in iv.data :
        #        print "offending read:",rid
        #first check if intervals are consistent with what is expected
        
        #Error logging will be based on the following cases
        #Case_Fresh: both new intervals. pass
        #Case_RefOverlap: overlaps on genome
        #Case_RefOverlap_2IntervalsEach: each interval has exactly two overlapping intervals . pass
        #Case_RefOverlap_itervalCountMismatch:  fail
        #Case_NoRefOverlap: no genome overlap
        #Case_NoRefOverlap_1IntervalEach: each has one alignment. probably covered by cases above. but having it explicitly helps error checking
        #Case_Others: everything else
            
        if not intervals1 and not intervals2: #Case_Fresh
            
            logging.debug("fresh")
            freshIv1 = Interval(aln1.gstart,aln1.gend,[rid])
            freshIv2 = Interval(aln2.gstart,aln2.gend,[rid])
            intervalTree_b.add(freshIv1)
            intervalTree_b.add(freshIv2)
            
            
            
        elif Alignments.overlapsOnReference(aln1,aln2):
           
            logging.debug("genome overlap")
            if len(intervals1) == 2 and len(intervals2) == 2 :#and intervals1 == intervals2: #last one is too strict?
                
                print "genomeOverlap. not fresh with read:",rid
                iv1 = Interval(min(aln1.gstart,intervals1[0].begin) , max(aln1.gend,intervals1[0].end) , intervals1[0].data + [rid])
                intervalTree_b.remove(intervals1[0])
                
                intervalTree_b.add(iv1)
                iv2 = Interval(min(aln2.gstart,intervals2[1].begin) , max(aln2.gend,intervals2[1].end) , intervals2[1].data + [rid])
                intervalTree_b.remove(intervals2[1])
                intervalTree_b.add(iv2)
                
                
            else: 
               
                logging.debug("RefOverlap_itervalCountMismatch. Read, aln1, aln2, %s %s %s"%(rid,aln1,aln2))
                logging.debug("Intervals1:"%intervals1)
                logging.debug("Intervals2:"%intervals2)
                return
                
        elif len(intervals1) == 1 and len(intervals2) == 1: #Case_NoRefOverlap_1IntervalEach
            
            logging.debug("no genome overlap, but not fresh")
            print "no genome Overlap.  not fresh. with read:",rid
            iv1 = Interval(min(aln1.gstart,intervals1[0].begin),max(aln1.gend,intervals1[0].end),intervals1[0].data+[rid])
            intervalTree_b.remove(intervals1[0])
            intervalTree_b.add(iv1)
            
            iv2 = Interval(min(aln2.gstart,intervals2[0].begin),max(aln2.gend,intervals2[0].end),intervals2[0].data+[rid])
            intervalTree_b.remove(intervals2[0])
            intervalTree_b.add(iv2)
            
        else: #Case_Others. 
            
            logging.debug("Case Others. Read, aln1, aln2, %s %s %s"%(rid,aln1,aln2))
            logging.debug("Intervals1:"%intervals1)
            logging.debug("Intervals2:"%intervals2)
        
        

if __name__ == '__main__':
    
    t = time.localtime()
    timestamp = time.strftime('%b-%d-%Y_%H%M',t)
    log_file = "log_"+timestamp
    logging.basicConfig(filename = log_file,level=logging.DEBUG) 
    
    #mafFile = "../../../data/Venter/alignments/mismap-from-lastSplit-mimicLastDefault/split-out.n.r6.q18.j1.Q0.d96.m0.81.maf"
    mafFile = "../../../data/Venter/alignments/testing-intervals/fromlast/alns.0.32.maf"
    #mafFile = "../../../data/Venter/alignments/testing-intervals/fromlast/alns.fr512To958.maf"
    msg = MatrixSplitGraph()
    msg.SplitsFromMaf(mafFile, maxMismapProb = 0.5,mismapFromLAST = True, maxoverlap = 90, deltabp = 32, minscore = 96, maxhits = 50)
    
    
    
